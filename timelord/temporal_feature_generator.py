import pandas as pd
import numpy as np
import time


class TempFeatureGenerator(object):
    """
    Generates temporal features of operations on specified ranges of days
    1) Select data from period of specified number of days before the last measurement
    2) Group on ID
    3) Perform specified operations per ID on columns for which an operation is defined in the operations
    dictionary
    4) Create suiting column names
    5) Concat the dataframe to the dataframe of the previous period lengths to generate the final dataframe
    """

    def __init__(self):
        pass

    def create_temp_features(self, df, date_column, period_lengths, operations, id_col):
        """
        Creates temporal features per ID
        :param df: Dataframe containing all data, indexed on ID and timestamp
        :param date_column: Column containing the date
        :param date: Reference date, start point from where the temporal features are calculated
        :param period_lengths: A list of all different periods of days of which you want to make a temporal feature
        :param operations: A dictionary of the desired operations per column
        :param id_col: The name of the column containing the ID's
        :return: A dataframe containing the results of the operations on the columns per ID
        """
        start_time = time.time()
        for i, length_period in enumerate(period_lengths[::1]):
            df_period = self.__to_period_df(df, length_period, id_col, date_column)
            if df_period.empty:
                # The mock_df serves to generate the column names of the columns that contain nans since
                # they are based on no values (an empty df_period)
                df_period = self.__create_mock_df(df, id_col)
                agg_result = self.__period_to_temp_df(df_period, operations, length_period, id_col)
                # empty df that contains the IDs as an index
                empty_ids_df = pd.DataFrame(index=df[id_col].unique())
                # agg_result contains nan values for the temporal features that are based on no measurements
                agg_result = empty_ids_df.reindex_axis(empty_ids_df.columns.union(agg_result.columns), axis=1)
            else:
                # agg_result contains the values of the aggregations selected in operations
                agg_result = self.__period_to_temp_df(df_period, operations, length_period, id_col)
            if i == 0:
                all_aggregated = agg_result
            else:
                #Merge the new temporal feature in the df with the other temporal features
                all_aggregated = all_aggregated.join(agg_result, how='left')
        all_aggregated['msno'] = all_aggregated.index.values
        duration_time = time.time() - start_time
        print('Done creating temporal features in: %s' % duration_time)
        return all_aggregated

    def __to_period_df(self, df, length_period, id_col, date_column):
        """
        :param df: The dataframe on which the period dataframe is based
        :param length_period: The number of days of the period on which the new temporal feature
        will be based
        :param id_col: The name of the column containing the ID's
        :param date_column: Column containing the date
        :return: A dataframe containing the last length_period days of each ID
        """
        id_groups = df.groupby(id_col)
        df_period = pd.DataFrame()
        # per ID retrieve the data from the number of days specified in length_period before the last
        # measurement for that ID concatenate that to one dataframe for that certain period length
        for name, id_df in id_groups:
            last_date_period = id_df[date_column].max()
            first_date_period = last_date_period - pd.Timedelta(days=length_period)
            id_df_period = id_df.loc[
                (id_df[date_column] >= first_date_period) & (id_df[date_column] < last_date_period)]
            if id_df_period.empty:
                continue
            if df_period.empty:
                df_period = id_df_period
            else:
                df_period = pd.concat([df_period, id_df_period])
        return df_period

    def __create_mock_df(self, df , id_col):
        """
        :param df: The original dataframe containing all IDs and all original columns
        :param id_col: The name of the column containing the ID's
        :return: A dataframe containing one row with a zero value for each column
        """
        zero_array = np.zeros(len(df.columns.values))
        df_period = pd.DataFrame(columns=df.columns.values)
        df_period.loc[0] = zero_array
        df_period[id_col] = df[id_col][0]
        return df_period

    def __period_to_temp_df(self, df_period, operations, length_period, id_col):
        """
        Performs the aggregation on the dataframe containing the data of all IDs of one period
        :param df_period: Dataframe containing the values that are measured in the specified period
        :param operations: A dictionary of the desired operations per column
        :param length_period: Int specifying the length (in days) of the current period
        :return: Dataframe in which the operations have been performed on the columns, containing
        columns whose name is: feature + operation + length of the period
        """
        user_groups = df_period.groupby(id_col)
        agg_result = user_groups.agg(operations)
        agg_result.columns = [colname[0] + '_' + colname[1] + '_' + str(length_period) for colname in
                              agg_result.columns.values]
        if agg_result.shape[0] == 1:
            agg_result.iloc[0, :] = np.nan
        return agg_result


