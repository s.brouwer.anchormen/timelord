import time #Only used to time the duration
import pandas as pd
from multiprocessing import Pool

#from . import util

def mp_func(args):
    group, date_column, min_resolution, id_column, multiple_date_operations, interpolate_kwargs, fill_na_args = args
    TTS = TableTimeSeries()
    time_series_df = TTS.build_df(group, 
                                  date_column, 
                                  min_resolution, 
                                  set_date_as_index = False,
                                  multiple_date_operations = multiple_date_operations,
                                  interpolate_kwargs = interpolate_kwargs, 
                                  fill_na_args = fill_na_args)
    user_id = group[id_column].iloc[0]
    time_series_df[id_column] = [user_id for _ in range(len(time_series_df))]
    return time_series_df

resolution=pd.tseries.frequencies.Resolution()


class TableTimeSeries(object):
    """
    Converts a timestamped dataframe into a complete time series based on a provided resolution.
    Provided that the timestamp is in a Datetime object or pandas Timestamp object it does the following:
    1) Create a new date_range
    2) Convert it to strings
    3) Cut off the unwanted resolution from the Timestamp & Date column(e.g. 2017-01-01 00:00:00 --> 2017-01-01)
    4) Join the original DataFrame with the string Date range
    5) Convert the type of the joined column to Datetime
    6) Set it to the DataFrame index and drop the old column.
    7) Return the new DataFrame with a complete DatetimeIndex full of NaN's and NaT's.

    
    Parameters
    ----------
    None
    

    
    TODO: 
    """
    DEBUG=False
    
    def __init__(self):
        pass
    
    def __get_dates(self, ts, minimal_resolution = 'day'):
        """
        Filters off the unwanted resolution off a Timestamp.
        
        Parameters
        ----------
        ts : Datetimeindex or a Series of Timestamps that accept the datetime attributes.
        
        minimal_resolution : string
        Accepts the arguments: 'year', 'month', 'day', 'hour', 'minute', 'second'
        Example: if minimal resolution = 'day'
        The Timestamp 2017-01-01 00:00:00 becomes the following string: 2017-01-01
        
        Returns
        ----------
        new_string : list
        A list with new timestamps in the desired format as String
        """
        #Used to aggregate the timestamps on a certain resolution level.
        resolutions = ['year', 'month', 'day', 'hour', 'minute', 'second']
        sel_res = resolutions[:1 + resolutions.index(minimal_resolution)]  
        format_string = '{}-'*len(sel_res)
        func = lambda i: format_string.format(*[getattr(i, res) for res in sel_res])[:-1] #Instead use .to_period? https://stackoverflow.com/questions/40923820/pandas-timedelta-in-months
        return list(map(func, ts))


    def __get_date_range(self, df, date_column, min_resolution = 'day'):
        """
        Creates a date_range based on the earliest and latest occurence of a date in a Datetimeindex.
        
        Parameters
        ----------
        df : DataFrame
        The DataFrame that is used for the time series
        
        date_column : string
        The column that is used to create the new time series
        
        min_resolution : 'string'
        Accepts the arguments: 'year', 'month', 'day', 'hour', 'minute', 'second'

        Returns
        ----------
        date_range : a pandas date_range
        A list with new timestamps in the desired format as String
        """
        
       
        dates = df[date_column].dropna().drop_duplicates()
        date_range = pd.date_range(start = dates.min(),
                                   end = dates.max(), 
                                   dtype = 'datetime64[d]',
                                   freq = resolution.get_freq(min_resolution))
        return date_range

    def build_df(self, df, date_column, min_resolution='day', join_column_name = 'join_column_name', set_date_as_index = False, multiple_date_operations = True, interpolate_kwargs = None, fill_na_args = None): 
        """
        Creates a DataFrame with only the new date_range as column as new basis.
        The original DataFrame will be merged against that resulting in a DataFrame
        with a lot of empty values.
        
        Parameters
        ----------
        df : DataFrame
        The DataFrame that is used for the time series
        
        date_range : string
        The new date_range that is used to merge the DataFrame against.
        
        date_column : string
        The name of the column that has the dates
        
        min_resolution : string
        Accepts the arguments: 'year', 'month', 'day', 'hour', 'minute', 'second'
        
        join_column_date : string
            default = 'join_column_name'
        The argument that takes the name for the new column name. If None the date_column value will be used as the column_name to join the dataframes.
        
        #Not implemented
        set_date_as_index : Boolean 
            default = False
        If True the date column will also be set as DatetimeIndex.
        
        multiple_date_operations : Boolean or Dict
            default = True
        If True the default operator will be used: 'sum'. If False None will be done.
        If a dict is provided with a mapping for each column this will be forwarded and called with a .agg method for the respective columns. Unmapped columns _will_ be dropped silently.

        interpolate_kwargs : Dict
        default = None
        The kwargs that will be used as arguments for the `.interpolate()` method. Is skipped by default.
        
        fillna args : value
            default = None
        The value that will be used to fill the NaNs with.
        
        Returns
        ----------
        new_df : DataFrame object
        A new DataFrame object with the index as a complete DatetimeIndex and the gaps as NaN's and NaT's for further
        refinement.
        
        NOTE: 
        Double Dates: When multiple timestamps share the same timestamp they are duplicated on the same row.
        
        TODO: 
        Add support for weeks, quarters, seasons as frequency
        Add interpolate function (default True) --> add metadata to inform user that interpolation has occured?
        
        """
        
        temp_date_range = self.__get_date_range(df, date_column)
        date_range = self.__get_dates(temp_date_range, min_resolution)

        date_df = pd.DataFrame(date_range, columns = [join_column_name])
        date_column_values = self.__get_dates(df[date_column], minimal_resolution = min_resolution)
        df = df.assign(**{join_column_name: date_column_values})
        new_df = date_df.merge(right = df, 
                               on = join_column_name, 
                               how = 'left')
        new_df[join_column_name] = new_df[join_column_name].apply(lambda x: pd.to_datetime(x)) #Not necessary?
        #Move below to build_df?
        #Do groupby appliance before joining them? Significant speed-up, maybe loss of results?
        if multiple_date_operations == True:
            new_df = self.handle_multiple_dates(new_df, join_column_name)
        elif type(multiple_date_operations) == dict:
            new_df = self.handle_multiple_dates(new_df, join_column_name, operation_dict = multiple_date_operations)

        if interpolate_kwargs is not None:
            new_df = new_df.interpolate(**interpolate_kwargs)
            
        if fill_na_args is not None:
            new_df = new_df.fillna(fill_na_args)
        return new_df        
    
    def build_df_per_id(self, df, date_column, min_resolution, id_column, use_multiprocessing = False, custom_func = mp_func, multiple_date_operations = True, interpolate_kwargs = None, fill_na_args = None): 
        """
        Creates a DataFrame with only the new date_range as column as new basis, per ID.
        The original DataFrame will be merged against that resulting in a DataFrame
        with a lot of empty values, per ID.
        
        It utilizes the groupby and applies the mp_func function outside this class to build
        a complete time series per ID. Which uses the build_df function.
        
        def mp_func(args):
            group, date_column, min_resolution = args
            TTS = TableTimeSeries()
            return TTS.build_df(group, date_column, min_resolution)
        
        Parameters
        ----------
        df : DataFrame
        The DataFrame that is used for the time series

        date_column : 'string'
        The name of the column that has the dates
        
        min_resolution : 'string'
        Accepts the arguments: 'year', 'month', 'day', 'hour', 'minute', 'second'
        
        id_column : 'string'
        The name of the column that contains the ID
        
        use_multiprocessing : Boolean
            default = False
        Does the groupby apply with multiprocessing.

        Returns
        ----------
        new_df : DataFrame object
        A new DataFrame object with the index as ID and a Time Series DataFrame per ID and the gaps as NaN's and NaT's for further
        refinement.
        
        """
        s = time.time()
        grouped_df = df.groupby(id_column)[df.columns]
        if self.DEBUG:
            print("Done with grouping", time.time() - s)
        if use_multiprocessing:
            with Pool() as p:
                ret_list = p.map(custom_func, [(group, date_column, min_resolution, id_column, multiple_date_operations, interpolate_kwargs, fill_na_args) for _, group in grouped_df]) #Suboptimal
            if self.DEBUG:    
                print("Done with MultiProcessing", time.time() - s)
            
            new_df = pd.concat(ret_list)
        else:
            new_df = grouped_df.apply(lambda group: custom_func((group, date_column, min_resolution, id_column, multiple_date_operations, interpolate_kwargs, fill_na_args)))
        if self.DEBUG:
            print("Done", time.time() - s)
        return new_df
    
    def handle_multiple_dates(self, time_df, column = 'join_column_name', operator = 'sum', operation_dict = None):
        """
        WARNING: Needs more refinement. Do not use.
        Is this even useful here? Maybe a bit overkill?
        """      
        if operation_dict is None:
            new_df = getattr(time_df.groupby(column), operator)()
        else:
            new_df = time_df.groupby(column).agg(operation_dict)
        if self.DEBUG:
            print("handle_multiple_dates: Dropped columns: {}".format(time_df.shape[1] - new_df.shape[1]))
        return new_df
    
    def join_time_series_dfs_on_index(self, df1, df2, join_column_name = 'join_column_name', date_range_freq = 'd', set_as_index = False, min_date = None, max_date = None):
        #Assumes that the time series are in the index
        #Assumes that the time series are complete from begin to end
        #Assumes that they are in the same frequency
        #Assumes that the wish is to extend the date ranges with both the extremes.
        df1[join_column_name] = df1.index
        df2[join_column_name] = df2.index
        
        if min_date:
            df1 = df1.loc[df1.index >= min_date]
            df2 = df2.loc[df2.index >= min_date]
        if max_date:
            df1 = df1.loc[df1.index <= max_date]
            df2 = df2.loc[df2.index <= max_date]
            
        first_date = self.__get_first_date(df1, df2)
        last_date = self.__get_last_date(df1, df2)
        new_date_range = pd.date_range(start = first_date, end = last_date, freq = date_range_freq, dtype = 'datetime64[d]')
        #Construct the new dataframe to merge against 
        #NOTE: Maybe break apart?
        new_tdf = pd.DataFrame(new_date_range, columns = [join_column_name])
        new_tdf = new_tdf.merge(right = df1,
                                on = join_column_name,
                                how = 'left')

        new_tdf = new_tdf.merge(right = df2,
                                on = join_column_name,
                                how = 'left')
        if set_as_index:
            new_tdf.index = pd.DatetimeIndex(new_tdf[join_column_name], freq = date_range_freq)
            new_tdf = new_tdf.drop(join_column_name, axis = 1)
        return new_tdf
                
    def __get_first_date(self, df1, df2):
        if df1.index[0] > df2.index[0]:
            first_date = df2.index[0]
        else:
            first_date = df1.index[0]
        return first_date

    def __get_last_date(self, df1, df2):
        if df1.index[-1] > df2.index[-1]:
            last_date = df1.index[-1]
        else:
            last_date = df2.index[-1]
        return last_date
        
    
    def __get_rolling_mean_time_series(self, df, col_name, window = 10, min_resolution = 'day', method = 'mean'):
        """
        WARNING: Needs more refinement. Do not use.
        """
        groupby_df = getattr(df.groupby(min_resolution), method)()
        zdf = pd.DataFrame(groupby_df)
        zdf['placeholder_index_colname'] = zdf.index
        
        timeframe = self.build_df(zdf, 'placeholder_index_colname', freq)
        timeframe = timeframe.fillna(0.0).drop('placeholder_index_colname', axis = 1)
        return pd.rolling_mean(timeframe[col_name], 
                                window = window, 
                               freq = resolution.get_freq(min_resolution))
