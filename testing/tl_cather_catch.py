import coloredlogs
print('original colored.DEFAULT_LOG_FORMAT')
print(coloredlogs.DEFAULT_LOG_FORMAT)
print('setting to format to a stupid default format for easy comparison from a behave feature')
coloredlogs.DEFAULT_LOG_FORMAT = 'NOTIME %(hostname)s %(name)s[NOPROCES] %(levelname)s %(message)s'


import timelord as tl

try:
    zerodiv=1/0
except:
    tl.catcher.catch()

def err():
    try:
        zerodiv=1/0
    except:
        tl.catcher.catch()
        
err()


def ign():
    try:
        zerodiv=1/0
    except:
        tl.catcher.catch(ignore=[ZeroDivisionError],was_doing='nothing',helpfull_tips='just never do it again')
        
ign()

