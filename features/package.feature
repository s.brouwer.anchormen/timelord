Feature: Timelord  packaging
Scenario: Timelord import package
    Given a file named "run_feature_script.py" with:
            """
            import timelord
            print(timelord)
            """
    When I run "python run_feature_script.py"
    Then the command output should contain "<module 'timelord' from "
    And the command returncode is "0"
