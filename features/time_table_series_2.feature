Feature: Timelord Table Time series load
Scenario: Timelord Table Time series load
    Given a file named "run_script.py" with:
            """
            import pandas as pd
            import numpy as np
            import timelord as tl
            #this example does not use tl yet.
            from timelord.util import Util

            import os
            import re
            
            path = '../test_data'
            review_df = pd.read_csv(os.path.join(path, 'amazon_reviews.csv')).drop('Unnamed: 0', axis = 1)
            
            #review_df.head(3)

            #The date column isn't in the right format yet so we cast it into a datetime series.
            # From the head it looks like unix datetime
            review_df['Time'] = review_df['Time'].apply(lambda x: pd.to_datetime(x, unit = 's'))
            
            
            def agg_func(x):
                return list(x)

            dtype_func_map = {'int': 'mean', #Input
                              'object': agg_func,
                              'datetime': 'unique'}

            multiple_date_operations = Util.map_dtype_func_map(review_df, dtype_func_map)
            multiple_date_operations['Id'] = agg_func

            mdo=str(multiple_date_operations) #The desired output
            mdo=re.sub(' at 0x............>',' at 0xNofunid>',mdo)
            print(mdo)
            """
    Given a file named "run_and_save_output.sh" with:
            """
            python run_script.py > run_result.txt
            """        
    When I run "sh run_and_save_output.sh"
    When I run "cp ../test_data/expected_outputs/time_table_series_2_feature_expected_output.txt expected_output.txt"
    When I run "diff --report-identical-files expected_output.txt run_result.txt"
    Then the command output should contain "Files expected_output.txt and run_result.txt are identical"
    And the command returncode is "0"
