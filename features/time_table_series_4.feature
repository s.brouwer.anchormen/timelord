Feature: Timelord Table Time series load
Scenario: Timelord Table Time series load
    Given a file named "run_script.py" with:
            """
            import pandas as pd
            import numpy as np
            import timelord as tl
            #this example does not use tl yet.
            from timelord.util import Util

            import os
            
            path = '../test_data'
            review_df = pd.read_csv(os.path.join(path, 'amazon_reviews.csv')).drop('Unnamed: 0', axis = 1)
            
            #review_df.head(3)

            #The date column isn't in the right format yet so we cast it into a datetime series.
            # From the head it looks like unix datetime
            review_df['Time'] = review_df['Time'].apply(lambda x: pd.to_datetime(x, unit = 's'))
            
            
            def agg_func(x):
                return list(x)

            INPUT_DF = review_df #The dataframe to which the columns must be matched
            dtype_func_map = {'int': 'mean', #Input
                              'object': agg_func,
                              'datetime': 'unique'}

            multiple_date_operations = Util.map_dtype_func_map(INPUT_DF, dtype_func_map)
            multiple_date_operations['Id'] = agg_func

            TTS=tl.table_timeseries.TableTimeSeries()
            new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'day',
                     multiple_date_operations = multiple_date_operations)
            
            print("HEAD3:")
            print(new_df.head(3))
            print("TAIL3:")
            print(new_df.tail(3))
            print("SHAPE:")
            print(new_df.shape)
            """
    Given a file named "run_and_save_output.sh" with:
            """
            python run_script.py > run_result.txt
            """        
    When I run "sh run_and_save_output.sh"
    When I run "cp ../test_data/expected_outputs/time_table_series_4_feature_expected_output.txt expected_output.txt"
    When I run "diff --report-identical-files expected_output.txt run_result.txt"
    Then the command output should contain "Files expected_output.txt and run_result.txt are identical"
    And the command returncode is "0"
