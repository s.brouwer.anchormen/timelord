
Feature: tl_cather_catch
Scenario: tl_cather_catch
    Given a file named "run_script.py" with:
        """
        import coloredlogs
        print('original colored.DEFAULT_LOG_FORMAT')
        print(coloredlogs.DEFAULT_LOG_FORMAT)
        print('setting to format to a stupid default format for easy comparison from a behave feature')
        coloredlogs.DEFAULT_LOG_FORMAT = 'NOTIME NOHOSTNAME %(name)s[NOPROCES] %(levelname)s %(message)s'
        
        
        import timelord as tl
        
        try:
            zerodiv=1/0
        except:
            tl.catcher.catch()
        
        def err():
            try:
                zerodiv=1/0
            except:
                tl.catcher.catch()
                
        err()
        
        
        def ign():
            try:
                zerodiv=1/0
            except:
                tl.catcher.catch(ignore=[ZeroDivisionError],was_doing='nothing',helpfull_tips='just never do it again')
                
        ign()
        

        """
    Given a file named "run_and_save_output.sh" with:
        """
        python run_script.py > run_result.txt 2>&1
        """        
    When I run "sh run_and_save_output.sh"
    When I run "cp ../test_data/expected_outputs/tl_cather_catch_expected_output.txt expected_output.txt"
    When I run "diff --report-identical-files expected_output.txt run_result.txt"
    Then the command output should contain "Files expected_output.txt and run_result.txt are identical"
    And the command returncode is "0"

