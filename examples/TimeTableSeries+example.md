

```python
import sys
new_p = '/home/casper/repos2/timelord'
if new_p not in sys.path:
    sys.path.append(new_p)

    


```


```python
#Imports
import os
import pandas as pd

path = '/home/casper/repos2/timelord_example_notebooks/example_data/'
review_df = pd.read_csv(os.path.join(path, 'amazon_reviews.csv')).drop('Unnamed: 0', axis = 1)
review_df.head(3)


#The date column isn't in the right format yet so we cast it into a datetime series. From the head it looks like unix datetime
review_df['Time'] = review_df['Time'].apply(lambda x: pd.to_datetime(x, unit = 's'))
review_df
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>B001E4KFG0</td>
      <td>A3SGXH7AUHU8GW</td>
      <td>delmartian</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>2011-04-27</td>
      <td>Good Quality Dog Food</td>
      <td>I have bought several of the Vitality canned d...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>B00813GRG4</td>
      <td>A1D87F6ZCVE5NK</td>
      <td>dll pa</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>2012-09-07</td>
      <td>Not as Advertised</td>
      <td>Product arrived labeled as Jumbo Salted Peanut...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>B000LQOCH0</td>
      <td>ABXLMWJIXXAIN</td>
      <td>Natalia Corres "Natalia Corres"</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
      <td>2008-08-18</td>
      <td>"Delight" says it all</td>
      <td>This is a confection that has been around a fe...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>B000UA0QIQ</td>
      <td>A395BORC6FGVXV</td>
      <td>Karl</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>2011-06-13</td>
      <td>Cough Medicine</td>
      <td>If you are looking for the secret ingredient i...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>B006K2ZZ7K</td>
      <td>A1UQRSCLF8GW1T</td>
      <td>Michael D. Bigham "M. Wassir"</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-10-21</td>
      <td>Great taffy</td>
      <td>Great taffy at a great price.  There was a wid...</td>
    </tr>
    <tr>
      <th>5</th>
      <td>6</td>
      <td>B006K2ZZ7K</td>
      <td>ADT0SRK1MGOEU</td>
      <td>Twoapennything</td>
      <td>0</td>
      <td>0</td>
      <td>4</td>
      <td>2012-07-12</td>
      <td>Nice Taffy</td>
      <td>I got a wild hair for taffy and ordered this f...</td>
    </tr>
    <tr>
      <th>6</th>
      <td>7</td>
      <td>B006K2ZZ7K</td>
      <td>A1SP2KVKFXXRU1</td>
      <td>David C. Sullivan</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-06-20</td>
      <td>Great!  Just as good as the expensive brands!</td>
      <td>This saltwater taffy had great flavors and was...</td>
    </tr>
    <tr>
      <th>7</th>
      <td>8</td>
      <td>B006K2ZZ7K</td>
      <td>A3JRGQVEQN31IQ</td>
      <td>Pamela G. Williams</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-05-03</td>
      <td>Wonderful, tasty taffy</td>
      <td>This taffy is so good.  It is very soft and ch...</td>
    </tr>
    <tr>
      <th>8</th>
      <td>9</td>
      <td>B000E7L2R4</td>
      <td>A1MZYO9TZK0BBI</td>
      <td>R. James</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>2011-11-23</td>
      <td>Yay Barley</td>
      <td>Right now I'm mostly just sprouting this so my...</td>
    </tr>
    <tr>
      <th>9</th>
      <td>10</td>
      <td>B00171APVA</td>
      <td>A21BT40VZCCYT4</td>
      <td>Carol A. Reed</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-10-26</td>
      <td>Healthy Dog Food</td>
      <td>This is a very healthy dog food. Good for thei...</td>
    </tr>
    <tr>
      <th>10</th>
      <td>11</td>
      <td>B0001PB9FE</td>
      <td>A3HDKO7OW0QNK4</td>
      <td>Canadian Fan</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>2005-02-08</td>
      <td>The Best Hot Sauce in the World</td>
      <td>I don't know if it's the cactus or the tequila...</td>
    </tr>
    <tr>
      <th>11</th>
      <td>12</td>
      <td>B0009XLVG0</td>
      <td>A2725IB4YY9JEB</td>
      <td>A Poeng "SparkyGoHome"</td>
      <td>4</td>
      <td>4</td>
      <td>5</td>
      <td>2010-08-27</td>
      <td>My cats LOVE this "diet" food better than thei...</td>
      <td>One of my boys needed to lose some weight and ...</td>
    </tr>
    <tr>
      <th>12</th>
      <td>13</td>
      <td>B0009XLVG0</td>
      <td>A327PCT23YH90</td>
      <td>LT</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>2012-06-13</td>
      <td>My Cats Are Not Fans of the New Food</td>
      <td>My cats have been happily eating Felidae Plati...</td>
    </tr>
    <tr>
      <th>13</th>
      <td>14</td>
      <td>B001GVISJM</td>
      <td>A18ECVX2RJ7HUE</td>
      <td>willie "roadie"</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>2010-11-05</td>
      <td>fresh and greasy!</td>
      <td>good flavor! these came securely packed... the...</td>
    </tr>
    <tr>
      <th>14</th>
      <td>15</td>
      <td>B001GVISJM</td>
      <td>A2MUGFV2TDQ47K</td>
      <td>Lynrie "Oh HELL no"</td>
      <td>4</td>
      <td>5</td>
      <td>5</td>
      <td>2010-03-12</td>
      <td>Strawberry Twizzlers - Yummy</td>
      <td>The Strawberry Twizzlers are my guilty pleasur...</td>
    </tr>
    <tr>
      <th>15</th>
      <td>16</td>
      <td>B001GVISJM</td>
      <td>A1CZX3CP8IKQIJ</td>
      <td>Brian A. Lee</td>
      <td>4</td>
      <td>5</td>
      <td>5</td>
      <td>2009-12-29</td>
      <td>Lots of twizzlers, just what you expect.</td>
      <td>My daughter loves twizzlers and this shipment ...</td>
    </tr>
    <tr>
      <th>16</th>
      <td>17</td>
      <td>B001GVISJM</td>
      <td>A3KLWF6WQ5BNYO</td>
      <td>Erica Neathery</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
      <td>2012-09-20</td>
      <td>poor taste</td>
      <td>I love eating them and they are good for watch...</td>
    </tr>
    <tr>
      <th>17</th>
      <td>18</td>
      <td>B001GVISJM</td>
      <td>AFKW14U97Z6QO</td>
      <td>Becca</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-08-16</td>
      <td>Love it!</td>
      <td>I am very satisfied with my Twizzler purchase....</td>
    </tr>
    <tr>
      <th>18</th>
      <td>19</td>
      <td>B001GVISJM</td>
      <td>A2A9X58G2GTBLP</td>
      <td>Wolfee1</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-12-23</td>
      <td>GREAT SWEET CANDY!</td>
      <td>Twizzlers, Strawberry my childhood favorite ca...</td>
    </tr>
    <tr>
      <th>19</th>
      <td>20</td>
      <td>B001GVISJM</td>
      <td>A3IV7CL2C13K2U</td>
      <td>Greg</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-10-08</td>
      <td>Home delivered twizlers</td>
      <td>Candy was delivered very fast and was purchase...</td>
    </tr>
    <tr>
      <th>20</th>
      <td>21</td>
      <td>B001GVISJM</td>
      <td>A1WO0KGLPR5PV6</td>
      <td>mom2emma</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-08-16</td>
      <td>Always fresh</td>
      <td>My husband is a Twizzlers addict.  We've bough...</td>
    </tr>
    <tr>
      <th>21</th>
      <td>22</td>
      <td>B001GVISJM</td>
      <td>AZOF9E17RGZH8</td>
      <td>Tammy Anderson</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-06-25</td>
      <td>TWIZZLERS</td>
      <td>I bought these for my husband who is currently...</td>
    </tr>
    <tr>
      <th>22</th>
      <td>23</td>
      <td>B001GVISJM</td>
      <td>ARYVQL4N737A1</td>
      <td>Charles Brown</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-05-09</td>
      <td>Delicious product!</td>
      <td>I can remember buying this candy as a kid and ...</td>
    </tr>
    <tr>
      <th>23</th>
      <td>24</td>
      <td>B001GVISJM</td>
      <td>AJ613OLZZUG7V</td>
      <td>Mare's</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-05-04</td>
      <td>Twizzlers</td>
      <td>I love this candy.  After weight watchers I ha...</td>
    </tr>
    <tr>
      <th>24</th>
      <td>25</td>
      <td>B001GVISJM</td>
      <td>A22P2J09NJ9HKE</td>
      <td>S. Cabanaugh "jilly pepper"</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-01-20</td>
      <td>Please sell these in Mexico!!</td>
      <td>I have lived out of the US for over 7 yrs now,...</td>
    </tr>
    <tr>
      <th>25</th>
      <td>26</td>
      <td>B001GVISJM</td>
      <td>A3FONPR03H3PJS</td>
      <td>Deborah S. Linzer "Cat Lady"</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2010-10-29</td>
      <td>Twizzlers - Strawberry</td>
      <td>Product received is as advertised.&lt;br /&gt;&lt;br /&gt;...</td>
    </tr>
    <tr>
      <th>26</th>
      <td>27</td>
      <td>B001GVISJM</td>
      <td>A3RXAU2N8KV45G</td>
      <td>lady21</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>2012-03-25</td>
      <td>Nasty No flavor</td>
      <td>The candy is just red , No flavor . Just  plan...</td>
    </tr>
    <tr>
      <th>27</th>
      <td>28</td>
      <td>B001GVISJM</td>
      <td>AAAS38B98HMIK</td>
      <td>Heather Dube</td>
      <td>0</td>
      <td>1</td>
      <td>4</td>
      <td>2012-03-16</td>
      <td>Great Bargain for the Price</td>
      <td>I was so glad Amazon carried these batteries. ...</td>
    </tr>
    <tr>
      <th>28</th>
      <td>29</td>
      <td>B00144C10S</td>
      <td>A2F4LZVGFLD1OB</td>
      <td>DaisyH</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-06-05</td>
      <td>YUMMY!</td>
      <td>I got this for my Mum who is not diabetic but ...</td>
    </tr>
    <tr>
      <th>29</th>
      <td>30</td>
      <td>B0001PB9FY</td>
      <td>A3HDKO7OW0QNK4</td>
      <td>Canadian Fan</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>2005-02-08</td>
      <td>The Best Hot Sauce in the World</td>
      <td>I don't know if it's the cactus or the tequila...</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>970</th>
      <td>971</td>
      <td>B0002XIB2Y</td>
      <td>A2DRQBOGCTC5HP</td>
      <td>TL "TerryLea"</td>
      <td>2</td>
      <td>3</td>
      <td>5</td>
      <td>2007-10-07</td>
      <td>Best white gravy !</td>
      <td>Nothing easier. Nothing better. Even beats gra...</td>
    </tr>
    <tr>
      <th>971</th>
      <td>972</td>
      <td>B0002XIB2Y</td>
      <td>A1891H0TIXLDXA</td>
      <td>Donald M. Bridges</td>
      <td>1</td>
      <td>2</td>
      <td>5</td>
      <td>2009-08-15</td>
      <td>Pioneer Gravy is GREAT!</td>
      <td>I have used Pioneer Gravy for a number of year...</td>
    </tr>
    <tr>
      <th>972</th>
      <td>973</td>
      <td>B000F0G75W</td>
      <td>A3QOSBTILZ84HY</td>
      <td>Junie "finaj"</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2007-03-08</td>
      <td>Eco sugar</td>
      <td>Its great to be able to get sugar this way.Its...</td>
    </tr>
    <tr>
      <th>973</th>
      <td>974</td>
      <td>B001EQ4DVQ</td>
      <td>AHMCLSA49GPHK</td>
      <td>David Boston</td>
      <td>7</td>
      <td>7</td>
      <td>5</td>
      <td>2010-02-09</td>
      <td>More expensive online</td>
      <td>I am a coffee fanatic but have always stayed a...</td>
    </tr>
    <tr>
      <th>974</th>
      <td>975</td>
      <td>B001EQ4DVQ</td>
      <td>AXZTWAX77RHPG</td>
      <td>William Strickler</td>
      <td>3</td>
      <td>3</td>
      <td>4</td>
      <td>2007-11-14</td>
      <td>Very Convenient and far better than instant co...</td>
      <td>I don't think these coffee bags are as good as...</td>
    </tr>
    <tr>
      <th>975</th>
      <td>976</td>
      <td>B001EQ4DVQ</td>
      <td>A3RB2ZB2KGRZVE</td>
      <td>Jacki Gunter "good movie or book lover"</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>2009-05-31</td>
      <td>No-Pot Fresh Coffee</td>
      <td>I keep Regular bags and Decaf bags in my home ...</td>
    </tr>
    <tr>
      <th>976</th>
      <td>977</td>
      <td>B001EQ4DVQ</td>
      <td>A37FCSX7K7GEFT</td>
      <td>K. Li</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
      <td>2007-06-17</td>
      <td>Simple. Convenient</td>
      <td>I live in the dorms without a kitchen so makin...</td>
    </tr>
    <tr>
      <th>977</th>
      <td>978</td>
      <td>B001EQ4DVQ</td>
      <td>A680RUE1FDO8B</td>
      <td>Jerry Saperstein</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2009-11-29</td>
      <td>Brews an excellent cup of coffee quickly and e...</td>
      <td>My primary point with this review is to note t...</td>
    </tr>
    <tr>
      <th>978</th>
      <td>979</td>
      <td>B001EQ4DVQ</td>
      <td>A23Q6TN6OK1U51</td>
      <td>T.Kriel "World Traveller &amp; Mother of 4"</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2009-09-24</td>
      <td>Very Convenient</td>
      <td>We can't have any kind of coffee maker at work...</td>
    </tr>
    <tr>
      <th>979</th>
      <td>980</td>
      <td>B001EQ4DVQ</td>
      <td>A38FWXBFJX210N</td>
      <td>Terri</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2009-09-12</td>
      <td>Great taste</td>
      <td>These are great for at work, just pop the inst...</td>
    </tr>
    <tr>
      <th>980</th>
      <td>981</td>
      <td>B001EQ4DVQ</td>
      <td>A1AMCR6ISCN2Z6</td>
      <td>William C. Diemer "wcd53"</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2007-06-12</td>
      <td>DELISH!!!!</td>
      <td>Fast, easy and definitely delicious.  Makes a ...</td>
    </tr>
    <tr>
      <th>981</th>
      <td>982</td>
      <td>B004DTNJU2</td>
      <td>A1C3LSDRPLFZ62</td>
      <td>Hank Schwartz</td>
      <td>3</td>
      <td>3</td>
      <td>5</td>
      <td>2011-02-16</td>
      <td>new to rooibos tea</td>
      <td>I gave this tea a try to add variety to my tea...</td>
    </tr>
    <tr>
      <th>982</th>
      <td>983</td>
      <td>B004DTNJU2</td>
      <td>A4D9AIUP1NNIY</td>
      <td>HotRod</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-07-08</td>
      <td>Tasted better than loose leaf rooibos</td>
      <td>I tried this before having normal red tea so I...</td>
    </tr>
    <tr>
      <th>983</th>
      <td>984</td>
      <td>B004DTNJU2</td>
      <td>A1YQORA01YBBLS</td>
      <td>MD_JD</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-01-04</td>
      <td>Best matcha quality and price</td>
      <td>I previously tried other matcha products and w...</td>
    </tr>
    <tr>
      <th>984</th>
      <td>985</td>
      <td>B00473OV2E</td>
      <td>A7HKP7RBH88IU</td>
      <td>Dick Dipuccio</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2012-03-06</td>
      <td>The Best</td>
      <td>This is the very best marinade you can buy as ...</td>
    </tr>
    <tr>
      <th>985</th>
      <td>986</td>
      <td>B00473OV2E</td>
      <td>A13KWQGEI9MHG0</td>
      <td>Book Dame</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-06-09</td>
      <td>Moore's Marinade, Gluten, low sodium and MSG F...</td>
      <td>This is a great product for those looking for ...</td>
    </tr>
    <tr>
      <th>986</th>
      <td>987</td>
      <td>B0048IACB2</td>
      <td>A16UWS8G26PDN8</td>
      <td>Peter J. Koulikourdis</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2011-07-04</td>
      <td>Great Tasting Diet Tea with all Natural Ingred...</td>
      <td>My poland spring driver gave us this new tea t...</td>
    </tr>
    <tr>
      <th>987</th>
      <td>988</td>
      <td>B006F2NYI2</td>
      <td>A36GDATSF85X7W</td>
      <td>Matt Waz</td>
      <td>2</td>
      <td>2</td>
      <td>5</td>
      <td>2012-01-14</td>
      <td>Awesome Sauce</td>
      <td>This hot sauce is one of my favorites. Its a p...</td>
    </tr>
    <tr>
      <th>988</th>
      <td>989</td>
      <td>B006F2NYI2</td>
      <td>A36WE3X9G0DZUZ</td>
      <td>Gale P</td>
      <td>2</td>
      <td>2</td>
      <td>5</td>
      <td>2011-12-20</td>
      <td>love this hot sauce</td>
      <td>I was never much of a hot sauce fan until I ta...</td>
    </tr>
    <tr>
      <th>989</th>
      <td>990</td>
      <td>B006F2NYI2</td>
      <td>A2ZP8BJR5PYLG7</td>
      <td>sparrow</td>
      <td>2</td>
      <td>2</td>
      <td>5</td>
      <td>2011-12-19</td>
      <td>Great hot sauce!</td>
      <td>This is my favorite hot sauce! I liketo use it...</td>
    </tr>
    <tr>
      <th>990</th>
      <td>991</td>
      <td>B006F2NYI2</td>
      <td>AFN9QJ7IE89N4</td>
      <td>C Bonner</td>
      <td>2</td>
      <td>2</td>
      <td>5</td>
      <td>2011-12-16</td>
      <td>Love This Stuff</td>
      <td>They have this in a local diner that I eat in ...</td>
    </tr>
    <tr>
      <th>991</th>
      <td>992</td>
      <td>B006F2NYI2</td>
      <td>AN85IOOEI9IYB</td>
      <td>Jdub</td>
      <td>2</td>
      <td>2</td>
      <td>5</td>
      <td>2011-12-13</td>
      <td>Best sauce around</td>
      <td>I was browsing around on Amazon looking for gi...</td>
    </tr>
    <tr>
      <th>992</th>
      <td>993</td>
      <td>B006F2NYI2</td>
      <td>AHSURYHM6ZZXC</td>
      <td>nuttmeg</td>
      <td>2</td>
      <td>2</td>
      <td>5</td>
      <td>2011-12-13</td>
      <td>Tasty hot sauce!</td>
      <td>This is my favorite hot sauce. I buy it locall...</td>
    </tr>
    <tr>
      <th>993</th>
      <td>994</td>
      <td>B006F2NYI2</td>
      <td>A1KFUVZ3BZ59R1</td>
      <td>merplinger</td>
      <td>2</td>
      <td>2</td>
      <td>5</td>
      <td>2011-12-13</td>
      <td>Best all around hot sauce</td>
      <td>So far I have had the habanero and the medium ...</td>
    </tr>
    <tr>
      <th>994</th>
      <td>995</td>
      <td>B006F2NYI2</td>
      <td>A1T5CH6SHV989P</td>
      <td>a biemold</td>
      <td>2</td>
      <td>2</td>
      <td>5</td>
      <td>2011-12-13</td>
      <td>best hot sauce around</td>
      <td>absolutely love the habenaro sauce...use it on...</td>
    </tr>
    <tr>
      <th>995</th>
      <td>996</td>
      <td>B006F2NYI2</td>
      <td>A1D3F6UI1RTXO0</td>
      <td>Swopes</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>2012-03-16</td>
      <td>Hot &amp; Flavorful</td>
      <td>BLACK MARKET HOT SAUCE IS WONDERFUL.... My hus...</td>
    </tr>
    <tr>
      <th>996</th>
      <td>997</td>
      <td>B006F2NYI2</td>
      <td>AF50D40Y85TV3</td>
      <td>Mike A.</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>2012-02-02</td>
      <td>Great Hot Sauce and people who run it!</td>
      <td>Man what can i say, this salsa is the bomb!! i...</td>
    </tr>
    <tr>
      <th>997</th>
      <td>998</td>
      <td>B006F2NYI2</td>
      <td>A3G313KLWDG3PW</td>
      <td>kefka82</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>2011-12-19</td>
      <td>this sauce is the shiznit</td>
      <td>this sauce is so good with just about anything...</td>
    </tr>
    <tr>
      <th>998</th>
      <td>999</td>
      <td>B006F2NYI2</td>
      <td>A3NIDDT7E7JIFW</td>
      <td>V. B. Brookshaw</td>
      <td>1</td>
      <td>2</td>
      <td>1</td>
      <td>2012-05-04</td>
      <td>Not Hot</td>
      <td>Not hot at all. Like the other low star review...</td>
    </tr>
    <tr>
      <th>999</th>
      <td>1000</td>
      <td>B006F2NYI2</td>
      <td>A132DJVI37RB4X</td>
      <td>Scottdrum</td>
      <td>2</td>
      <td>5</td>
      <td>2</td>
      <td>2012-03-22</td>
      <td>Not hot, not habanero</td>
      <td>I have to admit, I was a sucker for the large ...</td>
    </tr>
  </tbody>
</table>
<p>1000 rows × 10 columns</p>
</div>




```python
#EXAMPLE 1: Create a multiple_date operations_dict based on the types in 
#Goal: Test the "map_dtype_func_map" method
#Input: review_df
#output column_func_map

%load_ext autoreload
%autoreload

from src.util import Util

def agg_func(x):
    return list(x)

INPUT_DF = review_df #The dataframe to which the columns must be matched
dtype_func_map = {'int': 'mean', #Input
                 'object': agg_func,
                 'datetime': 'unique'}

multiple_date_operations = Util.map_dtype_func_map(INPUT_DF, dtype_func_map)
multiple_date_operations['Id'] = agg_func

multiple_date_operations #The desired output
```

    The autoreload extension is already loaded. To reload it, use:
      %reload_ext autoreload





    {'HelpfulnessDenominator': 'mean',
     'HelpfulnessNumerator': 'mean',
     'Id': <function __main__.agg_func>,
     'ProductId': <function __main__.agg_func>,
     'ProfileName': <function __main__.agg_func>,
     'Score': 'mean',
     'Summary': <function __main__.agg_func>,
     'Text': <function __main__.agg_func>,
     'Time': 'unique',
     'UserId': <function __main__.agg_func>}




```python
#EXAMPLE 2: Test the multiple date operations

#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'day',
                     multiple_date_operations = multiple_date_operations)
new_df#The desired output
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
    <tr>
      <th>join_column_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2005-02-08</th>
      <td>[11.0, 30.0]</td>
      <td>[B0001PB9FE, B0001PB9FY]</td>
      <td>[A3HDKO7OW0QNK4, A3HDKO7OW0QNK4]</td>
      <td>[Canadian Fan, Canadian Fan]</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>5.000000</td>
      <td>[2005-02-08T00:00:00.000000000]</td>
      <td>[The Best Hot Sauce in the World, The Best Hot...</td>
      <td>[I don't know if it's the cactus or the tequil...</td>
    </tr>
    <tr>
      <th>2005-02-09</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-10</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-11</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-12</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-13</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-14</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-15</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-16</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-17</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-18</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-19</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-20</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-21</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-22</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-23</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-24</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-25</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-26</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-27</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-02-28</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-01</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-02</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-03</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-04</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-05</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-06</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-07</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-08</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2005-03-09</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2012-09-27</th>
      <td>[581.0, 756.0]</td>
      <td>[B000G6RYNE, B0035YE9CS]</td>
      <td>[A2A8KWCE8RKB9T, A3GK03NK0A7GHQ]</td>
      <td>[David Glazer, J. Lyman "amazon addict"]</td>
      <td>0.500000</td>
      <td>0.500000</td>
      <td>4.500000</td>
      <td>[2012-09-27T00:00:00.000000000]</td>
      <td>[Good chips, Better than anything in the super...</td>
      <td>[I don't know what to say that others have not...</td>
    </tr>
    <tr>
      <th>2012-09-28</th>
      <td>[775.0]</td>
      <td>[B005MZIJBU]</td>
      <td>[A1SNAYITRIW75P]</td>
      <td>[Rizearcher "CrazyAboutReviews"]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>[2012-09-28T00:00:00.000000000]</td>
      <td>[Horrible .. dont buy it]</td>
      <td>[The laddoos turned bad in a few days after we...</td>
    </tr>
    <tr>
      <th>2012-09-29</th>
      <td>[53.0, 755.0]</td>
      <td>[B000G6RPMY, B0035YE9CS]</td>
      <td>[A9L6L5H9BPEBO, A34O62IDVYW7X9]</td>
      <td>[Edwin C. Pauzer, Mark P. Stevens "Utard"]</td>
      <td>0.500000</td>
      <td>0.500000</td>
      <td>4.500000</td>
      <td>[2012-09-29T00:00:00.000000000]</td>
      <td>[You'll go nuts over Ass-Kickin' Peanuts., And...</td>
      <td>[This wasn't in stock the last time I looked. ...</td>
    </tr>
    <tr>
      <th>2012-09-30</th>
      <td>[769.0]</td>
      <td>[B0016J4QKO]</td>
      <td>[A1MEI40LLXKQF6]</td>
      <td>[S. Walton]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-09-30T00:00:00.000000000]</td>
      <td>[Best wafers]</td>
      <td>[I think these are my favorite cookies ever. R...</td>
    </tr>
    <tr>
      <th>2012-10-01</th>
      <td>[125.0]</td>
      <td>[B003SE19UK]</td>
      <td>[ASOEVZ99YWPVL]</td>
      <td>[PurpleGreen]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-10-01T00:00:00.000000000]</td>
      <td>[Healthy &amp; They LOVE It!]</td>
      <td>[My holistic vet recommended this, along with ...</td>
    </tr>
    <tr>
      <th>2012-10-02</th>
      <td>[308.0, 503.0]</td>
      <td>[B0064KOUNI, B000G6RYNE]</td>
      <td>[A215QEI3FPLVXU, A3L4OZ0U0A5UDH]</td>
      <td>[Carol L. Chmiel "Carol Chmiel", Linda Mullings]</td>
      <td>0.500000</td>
      <td>1.000000</td>
      <td>5.000000</td>
      <td>[2012-10-02T00:00:00.000000000]</td>
      <td>[VANILLA TOOTSIE ROLLS, Delicious!!]</td>
      <td>[You HAVE to try the VANILLA Tootsie Rolls!  T...</td>
    </tr>
    <tr>
      <th>2012-10-03</th>
      <td>[162.0, 580.0, 836.0]</td>
      <td>[B000ITVLE2, B000G6RYNE, B001ELL9X6]</td>
      <td>[A1K37I9HWF3SAI, A20K6K9TZGX7RQ, A38OVPNVBVUO9Y]</td>
      <td>[James R. Mckinney, Yoli, T. Burton "Hakalau T...</td>
      <td>0.666667</td>
      <td>0.666667</td>
      <td>4.333333</td>
      <td>[2012-10-03T00:00:00.000000000]</td>
      <td>[Not the greatest tasting.., Yoli, great organ...</td>
      <td>[I have always purchased Star-Kist tuna but th...</td>
    </tr>
    <tr>
      <th>2012-10-04</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-05</th>
      <td>[56.0, 650.0]</td>
      <td>[B002GWHC0G, B001EPPCNK]</td>
      <td>[A2EFAW1P3DRXWO, AY1EF0GOH80EK]</td>
      <td>[TJ Fairfax, Natasha Stryker]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>4.500000</td>
      <td>[2012-10-05T00:00:00.000000000]</td>
      <td>[Our guests love it!, Do not taste from bottle...</td>
      <td>[We roast these in a large cast iron pan on th...</td>
    </tr>
    <tr>
      <th>2012-10-06</th>
      <td>[158.0]</td>
      <td>[B0036VM05I]</td>
      <td>[A335SFRERY214N]</td>
      <td>[D. Melo "Character Connoisseur"]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-10-06T00:00:00.000000000]</td>
      <td>[Great tasting sea salt WITH iodine]</td>
      <td>[Perfect size sea salt for the table or the pi...</td>
    </tr>
    <tr>
      <th>2012-10-07</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-08</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-09</th>
      <td>[137.0, 579.0]</td>
      <td>[B002SRYRE8, B000G6RYNE]</td>
      <td>[A198FU6P1BVUNZ, AV51MFS5XJXFP]</td>
      <td>[Sarah, candy lady]</td>
      <td>0.500000</td>
      <td>0.500000</td>
      <td>5.000000</td>
      <td>[2012-10-09T00:00:00.000000000]</td>
      <td>[Tastes awesome &amp; looks beautiful, a good buy]</td>
      <td>[The BEST investment I've ever made for ginger...</td>
    </tr>
    <tr>
      <th>2012-10-10</th>
      <td>[332.0]</td>
      <td>[B007DJ0O9I]</td>
      <td>[A24GM452AKJTT9]</td>
      <td>[AMAZONGURL]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>[2012-10-10T00:00:00.000000000]</td>
      <td>[AWFUL]</td>
      <td>[I received the items in a timely manner. Upon...</td>
    </tr>
    <tr>
      <th>2012-10-11</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-12</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-13</th>
      <td>[169.0, 827.0]</td>
      <td>[B0064KO0BU, B003AO5DLO]</td>
      <td>[A34D61RQILOKIJ, A3IQFDK2A3APGE]</td>
      <td>[TampaBayGal, SH]</td>
      <td>0.500000</td>
      <td>0.500000</td>
      <td>3.000000</td>
      <td>[2012-10-13T00:00:00.000000000]</td>
      <td>[Not Banana Runts, Happy Dog]</td>
      <td>[Besides being smaller than runts, they look t...</td>
    </tr>
    <tr>
      <th>2012-10-14</th>
      <td>[248.0, 379.0]</td>
      <td>[B007TFONH0, B003YXWAF8]</td>
      <td>[A10IFYN5U6X20R, A3OOAICYUJT8EL]</td>
      <td>[onemagoo, Corvette &amp;#34;Terry&amp;#34;]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-10-14T00:00:00.000000000]</td>
      <td>[Best way to buy kcups, Heinz no more!]</td>
      <td>[Once you've tried some random sampler packs (...</td>
    </tr>
    <tr>
      <th>2012-10-15</th>
      <td>[762.0, 917.0]</td>
      <td>[B006JWQFC0, B000ER6YO0]</td>
      <td>[A1WWAQFIYLCL0Y, A3T7LFZQGL24A5]</td>
      <td>[Richard L. Costello "rbjrev", JuliaB]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>2.500000</td>
      <td>[2012-10-15T00:00:00.000000000]</td>
      <td>[IT'S A LAXATIVE, Great taste for a picky baby...</td>
      <td>[This item, WERTHER'S SUGAR FREE HARD CANDY, i...</td>
    </tr>
    <tr>
      <th>2012-10-16</th>
      <td>[149.0, 632.0, 965.0]</td>
      <td>[B0064KU9HO, B000G6RYNE, B005NEXK6Y]</td>
      <td>[AJFIH0DZYJF7T, A16Q0OV7CIPQC0, A33B2PH7Y4WC6]</td>
      <td>[Lisa Barton, Steffanee Holt, reviewer777]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>4.333333</td>
      <td>[2012-10-16T00:00:00.000000000]</td>
      <td>[Ehhh..., Love the smaller bags!, Great product!]</td>
      <td>[These remind me of dog treats I made once usi...</td>
    </tr>
    <tr>
      <th>2012-10-17</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-18</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-19</th>
      <td>[337.0]</td>
      <td>[B002SRAU80]</td>
      <td>[A156LLD3VZJGC2]</td>
      <td>[D. W. Knowlington]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-10-19T00:00:00.000000000]</td>
      <td>[The king of all seasoning salts.]</td>
      <td>[This is honestly THE BEST seasoning salt in a...</td>
    </tr>
    <tr>
      <th>2012-10-20</th>
      <td>[240.0, 319.0]</td>
      <td>[B0093NIWVO, B00283TPYE]</td>
      <td>[A1JT114SOITFFO, A1SH7OWQ98P6FI]</td>
      <td>[Dan &amp; Eileen, Colleen Clark]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-10-20T00:00:00.000000000]</td>
      <td>[OMG best chocolate jelly belly, Swiss Chalet]</td>
      <td>[Fresh,a great way to get a little chocolate i...</td>
    </tr>
    <tr>
      <th>2012-10-21</th>
      <td>[5.0, 39.0]</td>
      <td>[B006K2ZZ7K, B001EO5QW8]</td>
      <td>[A1UQRSCLF8GW1T, A2GHZ2UTV2B0CD]</td>
      <td>[Michael D. Bigham "M. Wassir", JERRY REITH]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>4.500000</td>
      <td>[2012-10-21T00:00:00.000000000]</td>
      <td>[Great taffy, GOOD WAY TO START THE DAY....]</td>
      <td>[Great taffy at a great price.  There was a wi...</td>
    </tr>
    <tr>
      <th>2012-10-22</th>
      <td>[221.0, 826.0]</td>
      <td>[B001EO5ZMO, B003AO5DLO]</td>
      <td>[A8D27L5Q5BLRC, A1SCDZZWN02VVN]</td>
      <td>[K. Kwan, Mickey &amp;amp; Moose]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-10-22T00:00:00.000000000]</td>
      <td>[A fragrant tea, GOOD FOOD!]</td>
      <td>[I drink this tea plain (without sugar or milk...</td>
    </tr>
    <tr>
      <th>2012-10-23</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-24</th>
      <td>[761.0]</td>
      <td>[B006JWQFC0]</td>
      <td>[A2C9XE9I8RSKNX]</td>
      <td>[J. Johnson]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-10-24T00:00:00.000000000]</td>
      <td>[Amazingly true to flavors...]</td>
      <td>[I'm pleased with these- wasn't sure how they'...</td>
    </tr>
    <tr>
      <th>2012-10-25</th>
      <td>[868.0]</td>
      <td>[B000VKYKTG]</td>
      <td>[A3LRR63WLQ3N3O]</td>
      <td>[YOToyo]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>[2012-10-25T00:00:00.000000000]</td>
      <td>[Doesn't taste as it should]</td>
      <td>[I think the Pocky did not get stored rightly....</td>
    </tr>
    <tr>
      <th>2012-10-26</th>
      <td>[10.0]</td>
      <td>[B00171APVA]</td>
      <td>[A21BT40VZCCYT4]</td>
      <td>[Carol A. Reed]</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>[2012-10-26T00:00:00.000000000]</td>
      <td>[Healthy Dog Food]</td>
      <td>[This is a very healthy dog food. Good for the...</td>
    </tr>
  </tbody>
</table>
<p>2818 rows × 10 columns</p>
</div>




```python
#EXAMPLE 3: Test the multiple date operations
#NOTICE THE DOUBLE DATES at the first 2 rows.
#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'day',
                     multiple_date_operations = False)
new_df#The desired output
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>join_column_name</th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2005-02-08</td>
      <td>11.0</td>
      <td>B0001PB9FE</td>
      <td>A3HDKO7OW0QNK4</td>
      <td>Canadian Fan</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2005-02-08</td>
      <td>The Best Hot Sauce in the World</td>
      <td>I don't know if it's the cactus or the tequila...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2005-02-08</td>
      <td>30.0</td>
      <td>B0001PB9FY</td>
      <td>A3HDKO7OW0QNK4</td>
      <td>Canadian Fan</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2005-02-08</td>
      <td>The Best Hot Sauce in the World</td>
      <td>I don't know if it's the cactus or the tequila...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2005-02-09</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2005-02-10</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2005-02-11</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>5</th>
      <td>2005-02-12</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>6</th>
      <td>2005-02-13</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>7</th>
      <td>2005-02-14</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>8</th>
      <td>2005-02-15</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>9</th>
      <td>2005-02-16</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>10</th>
      <td>2005-02-17</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>11</th>
      <td>2005-02-18</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>12</th>
      <td>2005-02-19</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>13</th>
      <td>2005-02-20</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>14</th>
      <td>2005-02-21</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>15</th>
      <td>2005-02-22</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>16</th>
      <td>2005-02-23</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>17</th>
      <td>2005-02-24</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>18</th>
      <td>2005-02-25</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>19</th>
      <td>2005-02-26</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>20</th>
      <td>2005-02-27</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>21</th>
      <td>2005-02-28</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>22</th>
      <td>2005-03-01</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>23</th>
      <td>2005-03-02</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>24</th>
      <td>2005-03-03</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>25</th>
      <td>2005-03-04</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>26</th>
      <td>2005-03-05</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>27</th>
      <td>2005-03-06</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>28</th>
      <td>2005-03-07</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>29</th>
      <td>2005-03-08</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>3053</th>
      <td>2012-10-06</td>
      <td>158.0</td>
      <td>B0036VM05I</td>
      <td>A335SFRERY214N</td>
      <td>D. Melo "Character Connoisseur"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-06</td>
      <td>Great tasting sea salt WITH iodine</td>
      <td>Perfect size sea salt for the table or the pic...</td>
    </tr>
    <tr>
      <th>3054</th>
      <td>2012-10-07</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3055</th>
      <td>2012-10-08</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3056</th>
      <td>2012-10-09</td>
      <td>137.0</td>
      <td>B002SRYRE8</td>
      <td>A198FU6P1BVUNZ</td>
      <td>Sarah</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-09</td>
      <td>Tastes awesome &amp; looks beautiful</td>
      <td>The BEST investment I've ever made for ginger....</td>
    </tr>
    <tr>
      <th>3057</th>
      <td>2012-10-09</td>
      <td>579.0</td>
      <td>B000G6RYNE</td>
      <td>AV51MFS5XJXFP</td>
      <td>candy lady</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2012-10-09</td>
      <td>a good buy</td>
      <td>This is a real good product.I love these chips...</td>
    </tr>
    <tr>
      <th>3058</th>
      <td>2012-10-10</td>
      <td>332.0</td>
      <td>B007DJ0O9I</td>
      <td>A24GM452AKJTT9</td>
      <td>AMAZONGURL</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>2012-10-10</td>
      <td>AWFUL</td>
      <td>I received the items in a timely manner. Upon ...</td>
    </tr>
    <tr>
      <th>3059</th>
      <td>2012-10-11</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3060</th>
      <td>2012-10-12</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3061</th>
      <td>2012-10-13</td>
      <td>169.0</td>
      <td>B0064KO0BU</td>
      <td>A34D61RQILOKIJ</td>
      <td>TampaBayGal</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>2012-10-13</td>
      <td>Not Banana Runts</td>
      <td>Besides being smaller than runts, they look th...</td>
    </tr>
    <tr>
      <th>3062</th>
      <td>2012-10-13</td>
      <td>827.0</td>
      <td>B003AO5DLO</td>
      <td>A3IQFDK2A3APGE</td>
      <td>SH</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-13</td>
      <td>Happy Dog</td>
      <td>My 7 year old Cocker Spaniel loves this food a...</td>
    </tr>
    <tr>
      <th>3063</th>
      <td>2012-10-14</td>
      <td>248.0</td>
      <td>B007TFONH0</td>
      <td>A10IFYN5U6X20R</td>
      <td>onemagoo</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-14</td>
      <td>Best way to buy kcups</td>
      <td>Once you've tried some random sampler packs (a...</td>
    </tr>
    <tr>
      <th>3064</th>
      <td>2012-10-14</td>
      <td>379.0</td>
      <td>B003YXWAF8</td>
      <td>A3OOAICYUJT8EL</td>
      <td>Corvette &amp;#34;Terry&amp;#34;</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-14</td>
      <td>Heinz no more!</td>
      <td>This is ridiculous.  The best gourmet ketchup ...</td>
    </tr>
    <tr>
      <th>3065</th>
      <td>2012-10-15</td>
      <td>762.0</td>
      <td>B006JWQFC0</td>
      <td>A1WWAQFIYLCL0Y</td>
      <td>Richard L. Costello "rbjrev"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>2012-10-15</td>
      <td>IT'S A LAXATIVE</td>
      <td>This item, WERTHER'S SUGAR FREE HARD CANDY, if...</td>
    </tr>
    <tr>
      <th>3066</th>
      <td>2012-10-15</td>
      <td>917.0</td>
      <td>B000ER6YO0</td>
      <td>A3T7LFZQGL24A5</td>
      <td>JuliaB</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>2012-10-15</td>
      <td>Great taste for a picky baby, but very thin co...</td>
      <td>My daughter was not a big fan of baby food, es...</td>
    </tr>
    <tr>
      <th>3067</th>
      <td>2012-10-16</td>
      <td>149.0</td>
      <td>B0064KU9HO</td>
      <td>AJFIH0DZYJF7T</td>
      <td>Lisa Barton</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>3.0</td>
      <td>2012-10-16</td>
      <td>Ehhh...</td>
      <td>These remind me of dog treats I made once usin...</td>
    </tr>
    <tr>
      <th>3068</th>
      <td>2012-10-16</td>
      <td>632.0</td>
      <td>B000G6RYNE</td>
      <td>A16Q0OV7CIPQC0</td>
      <td>Steffanee Holt</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-16</td>
      <td>Love the smaller bags!</td>
      <td>My 8 yo is a Kettle chip addict so I was so ha...</td>
    </tr>
    <tr>
      <th>3069</th>
      <td>2012-10-16</td>
      <td>965.0</td>
      <td>B005NEXK6Y</td>
      <td>A33B2PH7Y4WC6</td>
      <td>reviewer777</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-16</td>
      <td>Great product!</td>
      <td>I love Panang Curry but would rather avoid the...</td>
    </tr>
    <tr>
      <th>3070</th>
      <td>2012-10-17</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3071</th>
      <td>2012-10-18</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3072</th>
      <td>2012-10-19</td>
      <td>337.0</td>
      <td>B002SRAU80</td>
      <td>A156LLD3VZJGC2</td>
      <td>D. W. Knowlington</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-19</td>
      <td>The king of all seasoning salts.</td>
      <td>This is honestly THE BEST seasoning salt in al...</td>
    </tr>
    <tr>
      <th>3073</th>
      <td>2012-10-20</td>
      <td>240.0</td>
      <td>B0093NIWVO</td>
      <td>A1JT114SOITFFO</td>
      <td>Dan &amp; Eileen</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-20</td>
      <td>OMG best chocolate jelly belly</td>
      <td>Fresh,a great way to get a little chocolate in...</td>
    </tr>
    <tr>
      <th>3074</th>
      <td>2012-10-20</td>
      <td>319.0</td>
      <td>B00283TPYE</td>
      <td>A1SH7OWQ98P6FI</td>
      <td>Colleen Clark</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-20</td>
      <td>Swiss Chalet</td>
      <td>service was great, but I meant to buy Swiss Ch...</td>
    </tr>
    <tr>
      <th>3075</th>
      <td>2012-10-21</td>
      <td>5.0</td>
      <td>B006K2ZZ7K</td>
      <td>A1UQRSCLF8GW1T</td>
      <td>Michael D. Bigham "M. Wassir"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-21</td>
      <td>Great taffy</td>
      <td>Great taffy at a great price.  There was a wid...</td>
    </tr>
    <tr>
      <th>3076</th>
      <td>2012-10-21</td>
      <td>39.0</td>
      <td>B001EO5QW8</td>
      <td>A2GHZ2UTV2B0CD</td>
      <td>JERRY REITH</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>2012-10-21</td>
      <td>GOOD WAY TO START THE DAY....</td>
      <td>I WAS VISITING MY FRIEND NATE THE OTHER MORNIN...</td>
    </tr>
    <tr>
      <th>3077</th>
      <td>2012-10-22</td>
      <td>221.0</td>
      <td>B001EO5ZMO</td>
      <td>A8D27L5Q5BLRC</td>
      <td>K. Kwan</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-22</td>
      <td>A fragrant tea</td>
      <td>I drink this tea plain (without sugar or milk)...</td>
    </tr>
    <tr>
      <th>3078</th>
      <td>2012-10-22</td>
      <td>826.0</td>
      <td>B003AO5DLO</td>
      <td>A1SCDZZWN02VVN</td>
      <td>Mickey &amp;amp; Moose</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-22</td>
      <td>GOOD FOOD!</td>
      <td>We are Chocolate Moose &amp; Mickey Moose,&lt;br /&gt;&lt;b...</td>
    </tr>
    <tr>
      <th>3079</th>
      <td>2012-10-23</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3080</th>
      <td>2012-10-24</td>
      <td>761.0</td>
      <td>B006JWQFC0</td>
      <td>A2C9XE9I8RSKNX</td>
      <td>J. Johnson</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-24</td>
      <td>Amazingly true to flavors...</td>
      <td>I'm pleased with these- wasn't sure how they'd...</td>
    </tr>
    <tr>
      <th>3081</th>
      <td>2012-10-25</td>
      <td>868.0</td>
      <td>B000VKYKTG</td>
      <td>A3LRR63WLQ3N3O</td>
      <td>YOToyo</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>2.0</td>
      <td>2012-10-25</td>
      <td>Doesn't taste as it should</td>
      <td>I think the Pocky did not get stored rightly. ...</td>
    </tr>
    <tr>
      <th>3082</th>
      <td>2012-10-26</td>
      <td>10.0</td>
      <td>B00171APVA</td>
      <td>A21BT40VZCCYT4</td>
      <td>Carol A. Reed</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-26</td>
      <td>Healthy Dog Food</td>
      <td>This is a very healthy dog food. Good for thei...</td>
    </tr>
  </tbody>
</table>
<p>3083 rows × 11 columns</p>
</div>




```python
#EXAMPLE 4: Test the multiple date operations
#NOTICE THE dropped columns due to conflicting operations (it defaults to .sum which can't be done on objects
# so those are silently dropped)
#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'day',
                     multiple_date_operations = True)
new_df#The desired output
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Id</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
    </tr>
    <tr>
      <th>join_column_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2005-02-08</th>
      <td>41.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>2005-02-09</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-10</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-11</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-12</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-13</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-14</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-15</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-16</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-17</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-18</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-19</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-20</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-21</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-22</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-23</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-24</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-25</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-26</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-27</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-02-28</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-02</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-03</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-04</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-05</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-06</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-07</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-08</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2005-03-09</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2012-09-27</th>
      <td>1337.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>9.0</td>
    </tr>
    <tr>
      <th>2012-09-28</th>
      <td>775.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>2012-09-29</th>
      <td>808.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>9.0</td>
    </tr>
    <tr>
      <th>2012-09-30</th>
      <td>769.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>2012-10-01</th>
      <td>125.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>2012-10-02</th>
      <td>811.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>2012-10-03</th>
      <td>1578.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>13.0</td>
    </tr>
    <tr>
      <th>2012-10-04</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2012-10-05</th>
      <td>706.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>9.0</td>
    </tr>
    <tr>
      <th>2012-10-06</th>
      <td>158.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>2012-10-07</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2012-10-08</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2012-10-09</th>
      <td>716.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>2012-10-10</th>
      <td>332.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>2012-10-11</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2012-10-12</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2012-10-13</th>
      <td>996.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>2012-10-14</th>
      <td>627.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>2012-10-15</th>
      <td>1679.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>2012-10-16</th>
      <td>1746.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>13.0</td>
    </tr>
    <tr>
      <th>2012-10-17</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2012-10-18</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2012-10-19</th>
      <td>337.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>2012-10-20</th>
      <td>559.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>2012-10-21</th>
      <td>44.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>9.0</td>
    </tr>
    <tr>
      <th>2012-10-22</th>
      <td>1047.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>2012-10-23</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2012-10-24</th>
      <td>761.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>2012-10-25</th>
      <td>868.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>2012-10-26</th>
      <td>10.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
    </tr>
  </tbody>
</table>
<p>2818 rows × 4 columns</p>
</div>




```python
#EXAMPLE 5: Test the resolution on year.
#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'year',
                     multiple_date_operations = multiple_date_operations)
new_df#The desired output
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
    <tr>
      <th>join_column_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2005-01-01</th>
      <td>[11, 30, 11, 30, 11, 30, 11, 30, 11, 30, 11, 3...</td>
      <td>[B0001PB9FE, B0001PB9FY, B0001PB9FE, B0001PB9F...</td>
      <td>[A3HDKO7OW0QNK4, A3HDKO7OW0QNK4, A3HDKO7OW0QNK...</td>
      <td>[Canadian Fan, Canadian Fan, Canadian Fan, Can...</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>5.000000</td>
      <td>[2005-02-08T00:00:00.000000000]</td>
      <td>[The Best Hot Sauce in the World, The Best Hot...</td>
      <td>[I don't know if it's the cactus or the tequil...</td>
    </tr>
    <tr>
      <th>2006-01-01</th>
      <td>[33, 34, 682, 33, 34, 682, 33, 34, 682, 33, 34...</td>
      <td>[B001EO5QW8, B001EO5QW8, B000G6MBX2, B001EO5QW...</td>
      <td>[AOVROBZ8BNTP7, A3PMM0NFVEJGK9, A4AYT6I29WTPY,...</td>
      <td>[S. Potter, Megan "Bad at Nicknames", Chip Lov...</td>
      <td>12.000000</td>
      <td>12.333333</td>
      <td>4.333333</td>
      <td>[2006-11-13T00:00:00.000000000, 2006-12-17T00:...</td>
      <td>[Best of the Instant Oatmeals, Good Instant, A...</td>
      <td>[McCann's Instant Oatmeal is great if you must...</td>
    </tr>
    <tr>
      <th>2007-01-01</th>
      <td>[35, 37, 47, 49, 69, 70, 263, 322, 325, 340, 3...</td>
      <td>[B001EO5QW8, B001EO5QW8, B001EO5QW8, B001EO5QW...</td>
      <td>[A2EB6OGOWCRU5H, A1MYS9LFFBIYKM, AQLL2R1PPR46X...</td>
      <td>[CorbyJames, Abby Chase "gluten free", grumpyr...</td>
      <td>2.292308</td>
      <td>3.184615</td>
      <td>4.338462</td>
      <td>[2007-03-30T00:00:00.000000000, 2007-09-27T00:...</td>
      <td>[Great Irish oatmeal for those in a hurry!, Lo...</td>
      <td>[Instant oatmeal can become soggy the minute t...</td>
    </tr>
    <tr>
      <th>2008-01-01</th>
      <td>[3, 36, 45, 46, 48, 51, 63, 147, 214, 266, 267...</td>
      <td>[B000LQOCH0, B001EO5QW8, B001EO5QW8, B001EO5QW...</td>
      <td>[ABXLMWJIXXAIN, A2CI0RLADCRKPF, A2G7B7FKP2O2PU...</td>
      <td>[Natalia Corres "Natalia Corres", T. J. Ryan, ...</td>
      <td>1.403846</td>
      <td>2.067308</td>
      <td>4.163462</td>
      <td>[2008-08-18T00:00:00.000000000, 2008-05-11T00:...</td>
      <td>["Delight" says it all, satisfying, Great tast...</td>
      <td>[This is a confection that has been around a f...</td>
    </tr>
    <tr>
      <th>2009-01-01</th>
      <td>[16, 38, 44, 52, 71, 101, 102, 107, 109, 111, ...</td>
      <td>[B001GVISJM, B001EO5QW8, B001EO5QW8, B000G6RPM...</td>
      <td>[A1CZX3CP8IKQIJ, A3MGP2E1ZZ6GRB, A17DW6SUOC70D...</td>
      <td>[Brian A. Lee, Zardoz "focuspuller", Mother of...</td>
      <td>1.901786</td>
      <td>2.535714</td>
      <td>4.294643</td>
      <td>[2009-12-29T00:00:00.000000000, 2009-03-31T00:...</td>
      <td>[Lots of twizzlers, just what you expect., it'...</td>
      <td>[My daughter loves twizzlers and this shipment...</td>
    </tr>
    <tr>
      <th>2010-01-01</th>
      <td>[12, 14, 15, 26, 32, 43, 54, 72, 75, 76, 86, 1...</td>
      <td>[B0009XLVG0, B001GVISJM, B001GVISJM, B001GVISJ...</td>
      <td>[A2725IB4YY9JEB, A18ECVX2RJ7HUE, A2MUGFV2TDQ47...</td>
      <td>[A Poeng "SparkyGoHome", willie "roadie", Lynr...</td>
      <td>1.600000</td>
      <td>2.103030</td>
      <td>3.963636</td>
      <td>[2010-08-27T00:00:00.000000000, 2010-11-05T00:...</td>
      <td>[My cats LOVE this "diet" food better than the...</td>
      <td>[One of my boys needed to lose some weight and...</td>
    </tr>
    <tr>
      <th>2011-01-01</th>
      <td>[1, 4, 9, 19, 20, 21, 22, 23, 24, 25, 31, 40, ...</td>
      <td>[B001E4KFG0, B000UA0QIQ, B000E7L2R4, B001GVISJ...</td>
      <td>[A3SGXH7AUHU8GW, A395BORC6FGVXV, A1MZYO9TZK0BB...</td>
      <td>[delmartian, Karl, R. James, Wolfee1, Greg, mo...</td>
      <td>1.183406</td>
      <td>1.558952</td>
      <td>4.279476</td>
      <td>[2011-04-27T00:00:00.000000000, 2011-06-13T00:...</td>
      <td>[Good Quality Dog Food, Cough Medicine, Yay Ba...</td>
      <td>[I have bought several of the Vitality canned ...</td>
    </tr>
    <tr>
      <th>2012-01-01</th>
      <td>[2, 5, 6, 7, 8, 10, 13, 17, 18, 27, 28, 29, 39...</td>
      <td>[B00813GRG4, B006K2ZZ7K, B006K2ZZ7K, B006K2ZZ7...</td>
      <td>[A1D87F6ZCVE5NK, A1UQRSCLF8GW1T, ADT0SRK1MGOEU...</td>
      <td>[dll pa, Michael D. Bigham "M. Wassir", Twoape...</td>
      <td>0.465625</td>
      <td>0.637500</td>
      <td>4.143750</td>
      <td>[2012-09-07T00:00:00.000000000, 2012-10-21T00:...</td>
      <td>[Not as Advertised, Great taffy, Nice Taffy, G...</td>
      <td>[Product arrived labeled as Jumbo Salted Peanu...</td>
    </tr>
  </tbody>
</table>
</div>




```python
#EXAMPLE 6: Test the resolution on month.
#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'month',
                     multiple_date_operations = multiple_date_operations)
new_df#The desired output
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
    <tr>
      <th>join_column_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2005-02-01</th>
      <td>[11.0, 30.0, 11.0, 30.0, 11.0, 30.0, 11.0, 30....</td>
      <td>[B0001PB9FE, B0001PB9FY, B0001PB9FE, B0001PB9F...</td>
      <td>[A3HDKO7OW0QNK4, A3HDKO7OW0QNK4, A3HDKO7OW0QNK...</td>
      <td>[Canadian Fan, Canadian Fan, Canadian Fan, Can...</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>5.000000</td>
      <td>[2005-02-08T00:00:00.000000000]</td>
      <td>[The Best Hot Sauce in the World, The Best Hot...</td>
      <td>[I don't know if it's the cactus or the tequil...</td>
    </tr>
    <tr>
      <th>2005-03-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-04-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-05-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-06-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-07-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-08-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-09-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-10-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-11-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2005-12-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-01-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-02-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-03-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-04-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-05-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-06-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-07-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-08-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-09-01</th>
      <td>[682.0, 682.0, 682.0, 682.0, 682.0, 682.0, 682...</td>
      <td>[B000G6MBX2, B000G6MBX2, B000G6MBX2, B000G6MBX...</td>
      <td>[A4AYT6I29WTPY, A4AYT6I29WTPY, A4AYT6I29WTPY, ...</td>
      <td>[Chip Lover "Leisa", Chip Lover "Leisa", Chip ...</td>
      <td>4.000000</td>
      <td>5.000000</td>
      <td>5.000000</td>
      <td>[2006-09-18T00:00:00.000000000]</td>
      <td>[Amazing Taste, Best Chip Ever!, Amazing Taste...</td>
      <td>[I can't believe how wonderful this chip is! T...</td>
    </tr>
    <tr>
      <th>2006-10-01</th>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
      <td>[nan, nan, nan, nan, nan, nan, nan, nan, nan, ...</td>
    </tr>
    <tr>
      <th>2006-11-01</th>
      <td>[33.0, 33.0, 33.0, 33.0, 33.0, 33.0, 33.0, 33....</td>
      <td>[B001EO5QW8, B001EO5QW8, B001EO5QW8, B001EO5QW...</td>
      <td>[AOVROBZ8BNTP7, AOVROBZ8BNTP7, AOVROBZ8BNTP7, ...</td>
      <td>[S. Potter, S. Potter, S. Potter, S. Potter, S...</td>
      <td>19.000000</td>
      <td>19.000000</td>
      <td>4.000000</td>
      <td>[2006-11-13T00:00:00.000000000]</td>
      <td>[Best of the Instant Oatmeals, Best of the Ins...</td>
      <td>[McCann's Instant Oatmeal is great if you must...</td>
    </tr>
    <tr>
      <th>2006-12-01</th>
      <td>[34.0, 34.0, 34.0, 34.0, 34.0, 34.0, 34.0, 34....</td>
      <td>[B001EO5QW8, B001EO5QW8, B001EO5QW8, B001EO5QW...</td>
      <td>[A3PMM0NFVEJGK9, A3PMM0NFVEJGK9, A3PMM0NFVEJGK...</td>
      <td>[Megan "Bad at Nicknames", Megan "Bad at Nickn...</td>
      <td>13.000000</td>
      <td>13.000000</td>
      <td>4.000000</td>
      <td>[2006-12-17T00:00:00.000000000]</td>
      <td>[Good Instant, Good Instant, Good Instant, Goo...</td>
      <td>[This is a good instant oatmeal from the best ...</td>
    </tr>
    <tr>
      <th>2007-01-01</th>
      <td>[69.0, 322.0, 325.0, 340.0, 347.0, 69.0, 322.0...</td>
      <td>[B000E7VI7S, B000JEHAHS, B000JEHAHS, B000HKYP9...</td>
      <td>[A1KL2LAW08X6UQ, AJ7Q49CTJ1BOC, A2CU16YU1KEOUY...</td>
      <td>[calmnsense, 24fan, Hurricane Ridge "Chris", M...</td>
      <td>2.200000</td>
      <td>5.000000</td>
      <td>3.600000</td>
      <td>[2007-01-20T00:00:00.000000000, 2007-01-11T00:...</td>
      <td>[How much would you pay for a bag of chocolate...</td>
      <td>[If you're impulsive like me, then $6 is ok. D...</td>
    </tr>
    <tr>
      <th>2007-02-01</th>
      <td>[346.0, 346.0, 346.0, 346.0, 346.0, 346.0, 346...</td>
      <td>[B000HKYP9A, B000HKYP9A, B000HKYP9A, B000HKYP9...</td>
      <td>[A2B7A8FJBT0HDX, A2B7A8FJBT0HDX, A2B7A8FJBT0HD...</td>
      <td>[S. Nayeem "Hanaa's mommy", S. Nayeem "Hanaa's...</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>3.000000</td>
      <td>[2007-02-16T00:00:00.000000000]</td>
      <td>[I wasn't that impressed, I wasn't that impres...</td>
      <td>[The Munchkin fresh food feeder is an okay pro...</td>
    </tr>
    <tr>
      <th>2007-03-01</th>
      <td>[35.0, 344.0, 345.0, 501.0, 528.0, 566.0, 623....</td>
      <td>[B001EO5QW8, B000HKYP9A, B000HKYP9A, B000G6RYN...</td>
      <td>[A2EB6OGOWCRU5H, A3JBI5QDS32UEM, AMNCDXFZKL6MZ...</td>
      <td>[CorbyJames, J. Foreman, C. Lamberts, D. Gessw...</td>
      <td>3.888889</td>
      <td>5.111111</td>
      <td>4.444444</td>
      <td>[2007-03-30T00:00:00.000000000, 2007-03-10T00:...</td>
      <td>[Great Irish oatmeal for those in a hurry!, Gr...</td>
      <td>[Instant oatmeal can become soggy the minute t...</td>
    </tr>
    <tr>
      <th>2007-04-01</th>
      <td>[676.0, 950.0, 676.0, 950.0, 676.0, 950.0, 676...</td>
      <td>[B000G6MBX2, B000MTIYF2, B000G6MBX2, B000MTIYF...</td>
      <td>[A2UFC8M91UFRX9, A3MYS4YDU2QXCP, A2UFC8M91UFRX...</td>
      <td>[HH "h_squared", D. Lee, HH "h_squared", D. Le...</td>
      <td>2.500000</td>
      <td>3.000000</td>
      <td>5.000000</td>
      <td>[2007-04-30T00:00:00.000000000, 2007-04-03T00:...</td>
      <td>[Who needs salsa when chips taste this good?, ...</td>
      <td>[These are the best tasting tortilla chips I h...</td>
    </tr>
    <tr>
      <th>2007-05-01</th>
      <td>[343.0, 350.0, 615.0, 620.0, 675.0, 343.0, 350...</td>
      <td>[B000HKYP9A, B00067AD4U, B000G6RYNE, B000G6RYN...</td>
      <td>[A6VEEE9HOL004, A2LYOWC3FC73XK, A3RO3CGZ9MWA1I...</td>
      <td>[Jaime M "Luc's Mom", Michael Ekstrum "express...</td>
      <td>1.800000</td>
      <td>2.000000</td>
      <td>4.600000</td>
      <td>[2007-05-27T00:00:00.000000000, 2007-05-12T00:...</td>
      <td>[Only good for ice, excellent - exactly what I...</td>
      <td>[This product is impossible to wash and actual...</td>
    </tr>
    <tr>
      <th>2007-06-01</th>
      <td>[351.0, 715.0, 977.0, 981.0, 351.0, 715.0, 977...</td>
      <td>[B00067AD4U, B000G6MBX2, B001EQ4DVQ, B001EQ4DV...</td>
      <td>[A7DSY3M6P6RG3, A14B8M117EUBLK, A37FCSX7K7GEFT...</td>
      <td>[taza052007, Shelly Kenyon, K. Li, William C. ...</td>
      <td>0.250000</td>
      <td>0.250000</td>
      <td>4.750000</td>
      <td>[2007-06-13T00:00:00.000000000, 2007-06-01T00:...</td>
      <td>[These are the Best!, Best tortilla chips ever...</td>
      <td>[These chocolate covered espresso beans are wo...</td>
    </tr>
    <tr>
      <th>2007-07-01</th>
      <td>[70.0, 402.0, 499.0, 500.0, 524.0, 535.0, 680....</td>
      <td>[B000E7VI7S, B001ELL6O8, B000G6RYNE, B000G6RYN...</td>
      <td>[AWCBF2ZWIN57F, A12T0F58OKHCIW, A16YH487W9ZYO0...</td>
      <td>[C. Salcido, fishaholic "hearyeethis", Bruce G...</td>
      <td>2.333333</td>
      <td>3.333333</td>
      <td>4.222222</td>
      <td>[2007-07-30T00:00:00.000000000, 2007-07-10T00:...</td>
      <td>[pretzel haven!, poor item packaging, An indul...</td>
      <td>[this was sooooo deliscious but too bad i ate ...</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2010-05-01</th>
      <td>[76.0, 197.0, 245.0, 552.0, 659.0, 832.0, 884....</td>
      <td>[B001EPPI84, B0028C44Z0, B001EO5ZMY, B000G6RYN...</td>
      <td>[A27TZ4WBU7N0YF, AI7O5C9KBVSTF, A1W4LZ4VZDF29Y...</td>
      <td>[I. So, i&amp;#60;3pnutbutter, Guy S. Painter "gsp...</td>
      <td>2.571429</td>
      <td>3.428571</td>
      <td>3.000000</td>
      <td>[2010-05-28T00:00:00.000000000, 2010-05-17T00:...</td>
      <td>[No Tea Flavor, Altoids Smalls-Wintergreen, Ah...</td>
      <td>[No tea flavor at all. Just whole brunch of ar...</td>
    </tr>
    <tr>
      <th>2010-06-01</th>
      <td>[280.0, 288.0, 719.0, 743.0, 801.0, 872.0, 873...</td>
      <td>[B001D07IPG, B001UJEN6C, B000G6MBX2, B001BB3LW...</td>
      <td>[A3OM7Q1IVAGLUW, A1XM65S80UQ2MD, ADD87VB66Y0DQ...</td>
      <td>[Pertigal, Joseph Kagan, L. Pray "bargainqueen...</td>
      <td>3.333333</td>
      <td>3.666667</td>
      <td>4.000000</td>
      <td>[2010-06-02T00:00:00.000000000, 2010-06-11T00:...</td>
      <td>[Very tasty chips!, Great Natural Energy, Deli...</td>
      <td>[So many "healthy" products don't taste very g...</td>
    </tr>
    <tr>
      <th>2010-07-01</th>
      <td>[54.0, 198.0, 287.0, 289.0, 354.0, 396.0, 517....</td>
      <td>[B000G6RPMY, B0028C44Z0, B000H13270, B001UJEN6...</td>
      <td>[AQ9DWWYP2KJCQ, A4LL4KXLRBQHQ, AVQIRN6E7J7UA, ...</td>
      <td>[Roel Trevino "protomex", stillgoinstrong, imd...</td>
      <td>1.461538</td>
      <td>2.000000</td>
      <td>3.692308</td>
      <td>[2010-07-02T00:00:00.000000000, 2010-07-07T00:...</td>
      <td>[not ass kickin, Sugarfree..., Not mild enough...</td>
      <td>[we're used to spicy foods down here in south ...</td>
    </tr>
    <tr>
      <th>2010-08-01</th>
      <td>[12.0, 175.0, 225.0, 283.0, 341.0, 370.0, 413....</td>
      <td>[B0009XLVG0, B0025VRCJY, B001EO5ZMO, B001D07IP...</td>
      <td>[A2725IB4YY9JEB, A3PPOTV6659ATF, A3A4GAPXRX5SH...</td>
      <td>[A Poeng "SparkyGoHome", Jerald, markiz, Louie...</td>
      <td>1.000000</td>
      <td>1.333333</td>
      <td>3.583333</td>
      <td>[2010-08-27T00:00:00.000000000, 2010-08-03T00:...</td>
      <td>[My cats LOVE this "diet" food better than the...</td>
      <td>[One of my boys needed to lose some weight and...</td>
    </tr>
    <tr>
      <th>2010-09-01</th>
      <td>[143.0, 172.0, 195.0, 275.0, 284.0, 369.0, 536...</td>
      <td>[B001GVISJW, B001IUKD76, B0028C44Z0, B000LKZK7...</td>
      <td>[A1ROND7G1QTOFY, A2KVCXTQVN18KI, A32DDKLY942A8...</td>
      <td>[Karon Dixon, A. Martin "tony_fourbu", Printer...</td>
      <td>2.250000</td>
      <td>3.050000</td>
      <td>3.800000</td>
      <td>[2010-09-30T00:00:00.000000000, 2010-09-01T00:...</td>
      <td>[I love these!!!!!!!!, Ricore forever, Nice li...</td>
      <td>[I love these.........very tasty!!!!!!!!!!!  I...</td>
    </tr>
    <tr>
      <th>2010-10-01</th>
      <td>[26.0, 32.0, 43.0, 75.0, 307.0, 407.0, 453.0, ...</td>
      <td>[B001GVISJM, B003F6UO7K, B001EO5QW8, B001EPPI8...</td>
      <td>[A3FONPR03H3PJS, A31OQO709M20Y7, A16XFOYQSJREL...</td>
      <td>[Deborah S. Linzer "Cat Lady", Molly V. Smith ...</td>
      <td>0.666667</td>
      <td>1.388889</td>
      <td>3.888889</td>
      <td>[2010-10-29T00:00:00.000000000, 2010-10-14T00:...</td>
      <td>[Twizzlers - Strawberry, THIS IS MY TASTE..., ...</td>
      <td>[Product received is as advertised.&lt;br /&gt;&lt;br /...</td>
    </tr>
    <tr>
      <th>2010-11-01</th>
      <td>[14.0, 72.0, 119.0, 216.0, 315.0, 449.0, 450.0...</td>
      <td>[B001GVISJM, B001GVISJC, B003SE19UK, B002TDK0V...</td>
      <td>[A18ECVX2RJ7HUE, A3BDOAPSF96WGQ, AD1WWGMEQ8FWW...</td>
      <td>[willie "roadie", 2tall, Bob Rex, scott Smith,...</td>
      <td>1.000000</td>
      <td>1.526316</td>
      <td>4.105263</td>
      <td>[2010-11-05T00:00:00.000000000, 2010-11-09T00:...</td>
      <td>[fresh and greasy!, Bigger then other brands, ...</td>
      <td>[good flavor! these came securely packed... th...</td>
    </tr>
    <tr>
      <th>2010-12-01</th>
      <td>[86.0, 124.0, 184.0, 355.0, 391.0, 392.0, 448....</td>
      <td>[B0019CW0HE, B003SE19UK, B001KUUNP6, B001LMNXF...</td>
      <td>[A25BGFRHYHEZKK, A9GRWS6KP8SMA, A262Z0S6PT9U16...</td>
      <td>[Toby's mom, Anne Kirkwood, Lee Thombley, Prou...</td>
      <td>1.600000</td>
      <td>1.750000</td>
      <td>4.300000</td>
      <td>[2010-12-21T00:00:00.000000000, 2010-12-17T00:...</td>
      <td>[Great food!, Palatable and healthy, Perfect f...</td>
      <td>[We have three dogs and all of them love this ...</td>
    </tr>
    <tr>
      <th>2011-01-01</th>
      <td>[25.0, 99.0, 108.0, 179.0, 217.0, 264.0, 294.0...</td>
      <td>[B001GVISJM, B0019CW0HE, B001REEG6C, B0025VRCJ...</td>
      <td>[A22P2J09NJ9HKE, ABZ9F0D94YK45, A30835UIR6F8KB...</td>
      <td>[S. Cabanaugh "jilly pepper", Amazon-tron 3000...</td>
      <td>1.263158</td>
      <td>1.842105</td>
      <td>4.157895</td>
      <td>[2011-01-20T00:00:00.000000000, 2011-01-18T00:...</td>
      <td>[Please sell these in Mexico!!, Perfect for ou...</td>
      <td>[I have lived out of the US for over 7 yrs now...</td>
    </tr>
    <tr>
      <th>2011-02-01</th>
      <td>[31.0, 98.0, 112.0, 262.0, 272.0, 273.0, 274.0...</td>
      <td>[B003F6UO7K, B0019CW0HE, B0037LW78C, B002MV23X...</td>
      <td>[AFM0O9480F04W, A3UII2114114PI, A3VEYLW2KPZNGK...</td>
      <td>[Sherril, FuNky Faja "SiLkk", G. Simmons, Alyd...</td>
      <td>1.444444</td>
      <td>1.722222</td>
      <td>4.166667</td>
      <td>[2011-02-14T00:00:00.000000000, 2011-02-10T00:...</td>
      <td>[Great machine!, Great allergy sensitive dog f...</td>
      <td>[I have never been a huge coffee fan. However,...</td>
    </tr>
    <tr>
      <th>2011-03-01</th>
      <td>[84.0, 176.0, 196.0, 210.0, 293.0, 309.0, 374....</td>
      <td>[B0019CW0HE, B0025VRCJY, B0028C44Z0, B0009XLVG...</td>
      <td>[A1FD9E5C06UB6B, A2VYRRDJN2UKWB, A3B6S35IUGOGW...</td>
      <td>[BRENDA DEMERS, Dario Quinonez "aprilkid", Rut...</td>
      <td>1.285714</td>
      <td>1.357143</td>
      <td>4.428571</td>
      <td>[2011-03-25T00:00:00.000000000, 2011-03-13T00:...</td>
      <td>[Natural Balance Lamb and Rice, EXCELLENT LEMO...</td>
      <td>[While my dogs like all of the flavors that we...</td>
    </tr>
    <tr>
      <th>2011-04-01</th>
      <td>[1.0, 41.0, 42.0, 97.0, 142.0, 235.0, 236.0, 2...</td>
      <td>[B001E4KFG0, B001EO5QW8, B001EO5QW8, B0019CW0H...</td>
      <td>[A3SGXH7AUHU8GW, AQCY5KRO7489S, A1WK4ALVZDYPUE...</td>
      <td>[delmartian, Garrett, Dick Baldwin "christobe"...</td>
      <td>1.000000</td>
      <td>1.312500</td>
      <td>4.062500</td>
      <td>[2011-04-27T00:00:00.000000000, 2011-04-22T00:...</td>
      <td>[Good Quality Dog Food, Why wouldn't you buy o...</td>
      <td>[I have bought several of the Vitality canned ...</td>
    </tr>
    <tr>
      <th>2011-05-01</th>
      <td>[23.0, 24.0, 117.0, 311.0, 321.0, 368.0, 373.0...</td>
      <td>[B001GVISJM, B001GVISJM, B0026Y3YBK, B003YDP5P...</td>
      <td>[ARYVQL4N737A1, AJ613OLZZUG7V, A3P60QLFDDCHOY,...</td>
      <td>[Charles Brown, Mare's, Giordano "GB", MLadyJ,...</td>
      <td>1.000000</td>
      <td>1.117647</td>
      <td>4.529412</td>
      <td>[2011-05-09T00:00:00.000000000, 2011-05-04T00:...</td>
      <td>[Delicious product!, Twizzlers, Great cookies,...</td>
      <td>[I can remember buying this candy as a kid and...</td>
    </tr>
    <tr>
      <th>2011-06-01</th>
      <td>[4.0, 22.0, 106.0, 114.0, 223.0, 382.0, 540.0,...</td>
      <td>[B000UA0QIQ, B001GVISJM, B004K2IHUO, B0037LW78...</td>
      <td>[A395BORC6FGVXV, AZOF9E17RGZH8, A2O9G2521O626G...</td>
      <td>[Karl, Tammy Anderson, Rachel Westendorf, John...</td>
      <td>1.176471</td>
      <td>2.294118</td>
      <td>3.823529</td>
      <td>[2011-06-13T00:00:00.000000000, 2011-06-25T00:...</td>
      <td>[Cough Medicine, TWIZZLERS, The best, the best...</td>
      <td>[If you are looking for the secret ingredient ...</td>
    </tr>
    <tr>
      <th>2011-07-01</th>
      <td>[89.0, 121.0, 213.0, 222.0, 292.0, 591.0, 643....</td>
      <td>[B0019CW0HE, B003SE19UK, B0009XLVGA, B001EO5ZM...</td>
      <td>[A3IMXZE9FCUNOC, A3A52DETIFZNZS, A2LWOW7SO2QPM...</td>
      <td>[Dana, Sarah Wilson, JHexatthewheel, Sophia T,...</td>
      <td>2.133333</td>
      <td>2.533333</td>
      <td>4.066667</td>
      <td>[2011-07-26T00:00:00.000000000, 2011-07-02T00:...</td>
      <td>[Better life for you dog!, Great food., Nearly...</td>
      <td>[Natural Balance Dry Dog Food Lamb Meal and Br...</td>
    </tr>
    <tr>
      <th>2011-08-01</th>
      <td>[21.0, 85.0, 123.0, 129.0, 141.0, 180.0, 189.0...</td>
      <td>[B001GVISJM, B0019CW0HE, B003SE19UK, B003OB0IB...</td>
      <td>[A1WO0KGLPR5PV6, AK2CXHH9VRZ2A, A29SUR9R8ZU54B...</td>
      <td>[mom2emma, I. GLENN, Alfbo, B. Pearce, N. Pine...</td>
      <td>1.250000</td>
      <td>1.625000</td>
      <td>4.083333</td>
      <td>[2011-08-16T00:00:00.000000000, 2011-08-13T00:...</td>
      <td>[Always fresh, INCREASED MY DOGS ITCHING, Good...</td>
      <td>[My husband is a Twizzlers addict.  We've boug...</td>
    </tr>
    <tr>
      <th>2011-09-01</th>
      <td>[40.0, 110.0, 160.0, 178.0, 204.0, 207.0, 212....</td>
      <td>[B001EO5QW8, B001REEG6C, B000ITVLE2, B0025VRCJ...</td>
      <td>[AO80AC8313NIZ, AY12DBB0U420B, A395S4RA1X7BFB,...</td>
      <td>[kYpondman, Gary Peterson, Pen_Name123, qv240,...</td>
      <td>0.444444</td>
      <td>0.555556</td>
      <td>4.666667</td>
      <td>[2011-09-28T00:00:00.000000000, 2011-09-19T00:...</td>
      <td>[Wife's favorite Breakfast, My Idea of a Good ...</td>
      <td>[I ordered this for my wife as it was reccomen...</td>
    </tr>
    <tr>
      <th>2011-10-01</th>
      <td>[20.0, 61.0, 62.0, 256.0, 360.0, 361.0, 387.0,...</td>
      <td>[B001GVISJM, B004N5KULM, B004N5KULM, B0048IC32...</td>
      <td>[A3IV7CL2C13K2U, A1ZR8O62VSU4OK, A7ZK2A3VIW7X9...</td>
      <td>[Greg, Lisa J. Szlosek "lisa", Peggy, Jihan S....</td>
      <td>1.384615</td>
      <td>2.538462</td>
      <td>4.000000</td>
      <td>[2011-10-08T00:00:00.000000000, 2011-10-16T00:...</td>
      <td>[Home delivered twizlers, Better price for thi...</td>
      <td>[Candy was delivered very fast and was purchas...</td>
    </tr>
    <tr>
      <th>2011-11-01</th>
      <td>[9.0, 55.0, 57.0, 59.0, 60.0, 88.0, 96.0, 202....</td>
      <td>[B000E7L2R4, B002GWHC0G, B004N5KULM, B004N5KUL...</td>
      <td>[A1MZYO9TZK0BBI, A1ND7WC5LXOU48, A202WR509428V...</td>
      <td>[R. James, David Belton "Proteus 1", amateur a...</td>
      <td>0.782609</td>
      <td>0.956522</td>
      <td>4.608696</td>
      <td>[2011-11-23T00:00:00.000000000, 2011-11-28T00:...</td>
      <td>[Yay Barley, Roasts up a smooth brew, Awesome ...</td>
      <td>[Right now I'm mostly just sprouting this so m...</td>
    </tr>
    <tr>
      <th>2011-12-01</th>
      <td>[19.0, 81.0, 82.0, 120.0, 132.0, 154.0, 156.0,...</td>
      <td>[B001GVISJM, B0066DMI6Y, B0066DMI6Y, B003SE19U...</td>
      <td>[A2A9X58G2GTBLP, AB30HQTI5VOLR, AZLONLC8OZPEC,...</td>
      <td>[Wolfee1, Melinda Bishop, John W. Hollis, Dami...</td>
      <td>1.257143</td>
      <td>1.457143</td>
      <td>4.457143</td>
      <td>[2011-12-23T00:00:00.000000000, 2011-12-29T00:...</td>
      <td>[GREAT SWEET CANDY!, Delicious!, Great, Best C...</td>
      <td>[Twizzlers, Strawberry my childhood favorite c...</td>
    </tr>
    <tr>
      <th>2012-01-01</th>
      <td>[73.0, 83.0, 105.0, 116.0, 128.0, 150.0, 153.0...</td>
      <td>[B006SQBRMA, B003ZFRKGO, B004K2IHUO, B0037LW78...</td>
      <td>[ATYMT48R62ENE, A2VOZX7YBT0D6D, AL3E5V6MXO9B0,...</td>
      <td>[Marie, Johnnycakes "Johnnycakes", pionex1796,...</td>
      <td>1.166667</td>
      <td>1.309524</td>
      <td>4.452381</td>
      <td>[2012-01-20T00:00:00.000000000, 2012-01-04T00:...</td>
      <td>[Best ever latice tart, Forget Molecular Gastr...</td>
      <td>[I ordered two of these and two of raspberry l...</td>
    </tr>
    <tr>
      <th>2012-02-01</th>
      <td>[94.0, 95.0, 130.0, 131.0, 139.0, 239.0, 330.0...</td>
      <td>[B0019CW0HE, B0019CW0HE, B003OB0IB8, B003OB0IB...</td>
      <td>[A3AF72GP4GVRY1, A1DJAAKLPCJRZD, A28D854VMOEHG...</td>
      <td>[John D. Meara, wendy, Benny, RTyst, Gettin Re...</td>
      <td>0.700000</td>
      <td>1.150000</td>
      <td>3.950000</td>
      <td>[2012-02-24T00:00:00.000000000, 2012-02-19T00:...</td>
      <td>[Great Dog Food!, So convenient, It burns!, Am...</td>
      <td>[My golden retriever is one of the most picky ...</td>
    </tr>
    <tr>
      <th>2012-03-01</th>
      <td>[27.0, 28.0, 66.0, 100.0, 104.0, 115.0, 122.0,...</td>
      <td>[B001GVISJM, B001GVISJM, B005DUM9UQ, B0019CW0H...</td>
      <td>[A3RXAU2N8KV45G, AAAS38B98HMIK, A3TWF9DQ6MU87E...</td>
      <td>[lady21, Heather Dube, dhy4b, Melissa Benjamin...</td>
      <td>0.571429</td>
      <td>1.142857</td>
      <td>3.964286</td>
      <td>[2012-03-25T00:00:00.000000000, 2012-03-16T00:...</td>
      <td>[Nasty No flavor, Great Bargain for the Price,...</td>
      <td>[The candy is just red , No flavor . Just  pla...</td>
    </tr>
    <tr>
      <th>2012-04-01</th>
      <td>[50.0, 64.0, 65.0, 74.0, 87.0, 91.0, 92.0, 93....</td>
      <td>[B001EO5QW8, B005DUM9UQ, B005DUM9UQ, B0059WXJK...</td>
      <td>[A276999Y6VRSCQ, A3HR0ZZOFKQ97N, A2XIHNXODNZGV...</td>
      <td>[JMay, RLC, ChemProf "chem professor", Diana R...</td>
      <td>0.536585</td>
      <td>0.658537</td>
      <td>4.341463</td>
      <td>[2012-04-10T00:00:00.000000000, 2012-04-04T00:...</td>
      <td>[Same stuff, Hammer Nutrition 's Fizz Rocks!, ...</td>
      <td>[This is the same stuff you can buy at the big...</td>
    </tr>
    <tr>
      <th>2012-05-01</th>
      <td>[8.0, 58.0, 68.0, 113.0, 186.0, 192.0, 193.0, ...</td>
      <td>[B006K2ZZ7K, B004N5KULM, B005DUM9UQ, B0037LW78...</td>
      <td>[A3JRGQVEQN31IQ, ASCNNAJU6SXF8, A8OFFIAL6XTOH,...</td>
      <td>[Pamela G. Williams, S. Beck, Bill Shirer, Ald...</td>
      <td>0.451613</td>
      <td>0.741935</td>
      <td>4.193548</td>
      <td>[2012-05-03T00:00:00.000000000, 2012-05-05T00:...</td>
      <td>[Wonderful, tasty taffy, How can you go wrong!...</td>
      <td>[This taffy is so good.  It is very soft and c...</td>
    </tr>
    <tr>
      <th>2012-06-01</th>
      <td>[7.0, 13.0, 29.0, 166.0, 182.0, 190.0, 191.0, ...</td>
      <td>[B006K2ZZ7K, B0009XLVG0, B00144C10S, B003TQQKF...</td>
      <td>[A1SP2KVKFXXRU1, A327PCT23YH90, A2F4LZVGFLD1OB...</td>
      <td>[David C. Sullivan, LT, DaisyH, Amy, Monie, Gi...</td>
      <td>0.600000</td>
      <td>0.760000</td>
      <td>4.440000</td>
      <td>[2012-06-20T00:00:00.000000000, 2012-06-13T00:...</td>
      <td>[Great!  Just as good as the expensive brands!...</td>
      <td>[This saltwater taffy had great flavors and wa...</td>
    </tr>
    <tr>
      <th>2012-07-01</th>
      <td>[6.0, 126.0, 133.0, 138.0, 151.0, 233.0, 234.0...</td>
      <td>[B006K2ZZ7K, B003SE19UK, B003OB0IB8, B002SRYRE...</td>
      <td>[ADT0SRK1MGOEU, A1QAJ948PN36II, A56HS2VNURVJ0,...</td>
      <td>[Twoapennything, Winter Green, J. Espinoza "jc...</td>
      <td>0.045455</td>
      <td>0.090909</td>
      <td>4.000000</td>
      <td>[2012-07-12T00:00:00.000000000, 2012-07-11T00:...</td>
      <td>[Nice Taffy, Wonderful food - perfect for alle...</td>
      <td>[I got a wild hair for taffy and ordered this ...</td>
    </tr>
    <tr>
      <th>2012-08-01</th>
      <td>[18.0, 67.0, 80.0, 148.0, 168.0, 170.0, 171.0,...</td>
      <td>[B001GVISJM, B005DUM9UQ, B005R8JE8O, B0017I8UM...</td>
      <td>[AFKW14U97Z6QO, AEFIZIJ1FK1BK, A3H1EQD2PBC085,...</td>
      <td>[Becca, Doug, jmc, Matthew R Lehmkuhler, Nikki...</td>
      <td>0.175000</td>
      <td>0.250000</td>
      <td>3.850000</td>
      <td>[2012-08-16T00:00:00.000000000, 2012-08-17T00:...</td>
      <td>[Love it!, Low Carb Alternative to Gatorade, t...</td>
      <td>[I am very satisfied with my Twizzler purchase...</td>
    </tr>
    <tr>
      <th>2012-09-01</th>
      <td>[2.0, 17.0, 53.0, 77.0, 78.0, 79.0, 90.0, 118....</td>
      <td>[B00813GRG4, B001GVISJM, B000G6RPMY, B004X2KR3...</td>
      <td>[A1D87F6ZCVE5NK, A3KLWF6WQ5BNYO, A9L6L5H9BPEBO...</td>
      <td>[dll pa, Erica Neathery, Edwin C. Pauzer, Jess...</td>
      <td>0.150000</td>
      <td>0.175000</td>
      <td>3.875000</td>
      <td>[2012-09-07T00:00:00.000000000, 2012-09-20T00:...</td>
      <td>[Not as Advertised, poor taste, You'll go nuts...</td>
      <td>[Product arrived labeled as Jumbo Salted Peanu...</td>
    </tr>
    <tr>
      <th>2012-10-01</th>
      <td>[5.0, 10.0, 39.0, 56.0, 125.0, 137.0, 149.0, 1...</td>
      <td>[B006K2ZZ7K, B00171APVA, B001EO5QW8, B002GWHC0...</td>
      <td>[A1UQRSCLF8GW1T, A21BT40VZCCYT4, A2GHZ2UTV2B0C...</td>
      <td>[Michael D. Bigham "M. Wassir", Carol A. Reed,...</td>
      <td>0.161290</td>
      <td>0.193548</td>
      <td>4.290323</td>
      <td>[2012-10-21T00:00:00.000000000, 2012-10-26T00:...</td>
      <td>[Great taffy, Healthy Dog Food, GOOD WAY TO ST...</td>
      <td>[Great taffy at a great price.  There was a wi...</td>
    </tr>
  </tbody>
</table>
<p>93 rows × 10 columns</p>
</div>




```python
#EXAMPLE 7: build_df_per_id method: test without multiprocessing WITH multiple_date_operations = True
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = False,
                             multiple_date_operations = True)
new_df 
```

    Done with grouping 0.00022649765014648438
    Done 7.779759168624878





<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>Id</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>UserId</th>
    </tr>
    <tr>
      <th>UserId</th>
      <th>join_column_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A104Y49ZQ4CYJ2</th>
      <th>2012-08-13</th>
      <td>171.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A104Y49ZQ4CYJ2</td>
    </tr>
    <tr>
      <th>A108P30XVUFKXY</th>
      <th>2008-02-19</th>
      <td>51.0</td>
      <td>0.0</td>
      <td>7.0</td>
      <td>1.0</td>
      <td>A108P30XVUFKXY</td>
    </tr>
    <tr>
      <th>A10EHUTGNC4BGP</th>
      <th>2009-05-31</th>
      <td>109.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>A10EHUTGNC4BGP</td>
    </tr>
    <tr>
      <th>A10IFYN5U6X20R</th>
      <th>2012-10-14</th>
      <td>248.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A10IFYN5U6X20R</td>
    </tr>
    <tr>
      <th>A10RJEQN64ATXU</th>
      <th>2007-09-03</th>
      <td>560.0</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>A10RJEQN64ATXU</td>
    </tr>
    <tr>
      <th>A11LJJL1SOH6W0</th>
      <th>2011-01-29</th>
      <td>930.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>A11LJJL1SOH6W0</td>
    </tr>
    <tr>
      <th>A11QUNPSCNHY62</th>
      <th>2012-06-17</th>
      <td>664.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>A11QUNPSCNHY62</td>
    </tr>
    <tr>
      <th>A11UPZ6LI1UJZY</th>
      <th>2010-12-29</th>
      <td>932.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A11UPZ6LI1UJZY</td>
    </tr>
    <tr>
      <th>A11V8XN8SUHPOV</th>
      <th>2010-01-20</th>
      <td>750.0</td>
      <td>5.0</td>
      <td>10.0</td>
      <td>5.0</td>
      <td>A11V8XN8SUHPOV</td>
    </tr>
    <tr>
      <th>A11VPI6WLMQ2ZQ</th>
      <th>2007-12-31</th>
      <td>871.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A11VPI6WLMQ2ZQ</td>
    </tr>
    <tr>
      <th>A12OF4IM7A8YS</th>
      <th>2008-07-05</th>
      <td>481.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A12OF4IM7A8YS</td>
    </tr>
    <tr>
      <th>A12T0F58OKHCIW</th>
      <th>2007-07-10</th>
      <td>402.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>A12T0F58OKHCIW</td>
    </tr>
    <tr>
      <th>A132DJVI37RB4X</th>
      <th>2012-03-22</th>
      <td>1000.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2.0</td>
      <td>A132DJVI37RB4X</td>
    </tr>
    <tr>
      <th>A137DV4YVGWDCR</th>
      <th>2012-03-23</th>
      <td>317.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A137DV4YVGWDCR</td>
    </tr>
    <tr>
      <th>A139RTDNMU3WY5</th>
      <th>2012-06-18</th>
      <td>376.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>A139RTDNMU3WY5</td>
    </tr>
    <tr>
      <th>A13EBBWOMC0FUI</th>
      <th>2010-01-11</th>
      <td>936.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A13EBBWOMC0FUI</td>
    </tr>
    <tr>
      <th>A13G8CNY7VX57F</th>
      <th>2011-12-03</th>
      <td>850.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>A13G8CNY7VX57F</td>
    </tr>
    <tr>
      <th>A13HTFH4ZY0NCE</th>
      <th>2010-12-01</th>
      <td>787.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A13HTFH4ZY0NCE</td>
    </tr>
    <tr>
      <th>A13KWQGEI9MHG0</th>
      <th>2011-06-09</th>
      <td>986.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A13KWQGEI9MHG0</td>
    </tr>
    <tr>
      <th>A13SOXNMWXQVNU</th>
      <th>2009-01-11</th>
      <td>526.0</td>
      <td>4.0</td>
      <td>4.0</td>
      <td>5.0</td>
      <td>A13SOXNMWXQVNU</td>
    </tr>
    <tr>
      <th>A13T2G4T8LR8XA</th>
      <th>2007-09-09</th>
      <td>395.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>A13T2G4T8LR8XA</td>
    </tr>
    <tr>
      <th>A13VD7TJDWVOEC</th>
      <th>2009-11-16</th>
      <td>457.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A13VD7TJDWVOEC</td>
    </tr>
    <tr>
      <th>A1447CDAPZGLYV</th>
      <th>2012-01-14</th>
      <td>336.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>A1447CDAPZGLYV</td>
    </tr>
    <tr>
      <th>A1465JH39KR5O5</th>
      <th>2010-10-18</th>
      <td>453.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A1465JH39KR5O5</td>
    </tr>
    <tr>
      <th>A14B8M117EUBLK</th>
      <th>2007-06-01</th>
      <td>715.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A14B8M117EUBLK</td>
    </tr>
    <tr>
      <th>A14HZ5EMD2WCG</th>
      <th>2008-01-06</th>
      <td>550.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>3.0</td>
      <td>A14HZ5EMD2WCG</td>
    </tr>
    <tr>
      <th>A14SOW889A4TAT</th>
      <th>2009-12-29</th>
      <td>548.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>A14SOW889A4TAT</td>
    </tr>
    <tr>
      <th>A156LLD3VZJGC2</th>
      <th>2012-10-19</th>
      <td>337.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A156LLD3VZJGC2</td>
    </tr>
    <tr>
      <th>A15U6JW86FB9OV</th>
      <th>2008-06-28</th>
      <td>532.0</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>A15U6JW86FB9OV</td>
    </tr>
    <tr>
      <th>A15UN7OK3WRFXE</th>
      <th>2009-06-11</th>
      <td>699.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A15UN7OK3WRFXE</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="19" valign="top">AY1EF0GOH80EK</th>
      <th>2012-09-17</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-18</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-19</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-20</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-21</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-22</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-23</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-24</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-25</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-26</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-27</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-28</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-29</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-30</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-02</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-03</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-04</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-05</th>
      <td>650.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>AYAU6OMJ81Q7Z</th>
      <th>2012-09-19</th>
      <td>918.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AYAU6OMJ81Q7Z</td>
    </tr>
    <tr>
      <th>AYB4ELCS5AM8P</th>
      <th>2011-03-20</th>
      <td>444.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AYB4ELCS5AM8P</td>
    </tr>
    <tr>
      <th>AYBYYDVV5ABJE</th>
      <th>2009-08-26</th>
      <td>522.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>3.0</td>
      <td>AYBYYDVV5ABJE</td>
    </tr>
    <tr>
      <th>AYMV2T86WIXVD</th>
      <th>2008-09-07</th>
      <td>857.0</td>
      <td>5.0</td>
      <td>5.0</td>
      <td>4.0</td>
      <td>AYMV2T86WIXVD</td>
    </tr>
    <tr>
      <th>AYSEXV27DPQ3E</th>
      <th>2008-07-28</th>
      <td>607.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>AYSEXV27DPQ3E</td>
    </tr>
    <tr>
      <th>AYZZSRYAIXNOS</th>
      <th>2011-01-31</th>
      <td>899.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AYZZSRYAIXNOS</td>
    </tr>
    <tr>
      <th>AZ2NEHE8TNRUW</th>
      <th>2011-01-21</th>
      <td>644.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AZ2NEHE8TNRUW</td>
    </tr>
    <tr>
      <th>AZ3FPU1QSFBC6</th>
      <th>2011-03-13</th>
      <td>794.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>AZ3FPU1QSFBC6</td>
    </tr>
    <tr>
      <th>AZ7289G0ILRFF</th>
      <th>2008-04-29</th>
      <td>398.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>AZ7289G0ILRFF</td>
    </tr>
    <tr>
      <th>AZLONLC8OZPEC</th>
      <th>2011-12-23</th>
      <td>82.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>AZLONLC8OZPEC</td>
    </tr>
    <tr>
      <th>AZOF9E17RGZH8</th>
      <th>2011-06-25</th>
      <td>22.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AZOF9E17RGZH8</td>
    </tr>
  </tbody>
</table>
<p>4050 rows × 5 columns</p>
</div>




```python
#EXAMPLE 8: build_df_per_id method: test without multiprocessing WITH multiple_date_operations = False
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = False,
                             multiple_date_operations = False)
new_df 
```

    Done with grouping 0.00024199485778808594
    Done 6.054147958755493





<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>join_column_name</th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
    <tr>
      <th>UserId</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A104Y49ZQ4CYJ2</th>
      <th>0</th>
      <td>2012-08-13</td>
      <td>171.0</td>
      <td>B0064KO0BU</td>
      <td>A104Y49ZQ4CYJ2</td>
      <td>H. Adams "hollya"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-08-13</td>
      <td>Worked great!</td>
      <td>I purchased as a giveaway for a baby shower wi...</td>
    </tr>
    <tr>
      <th>A108P30XVUFKXY</th>
      <th>0</th>
      <td>2008-02-19</td>
      <td>51.0</td>
      <td>B001EO5QW8</td>
      <td>A108P30XVUFKXY</td>
      <td>Roberto A</td>
      <td>0.0</td>
      <td>7.0</td>
      <td>1.0</td>
      <td>2008-02-19</td>
      <td>Don't like it</td>
      <td>This oatmeal is not good. Its mushy, soft, I d...</td>
    </tr>
    <tr>
      <th>A10EHUTGNC4BGP</th>
      <th>0</th>
      <td>2009-05-31</td>
      <td>109.0</td>
      <td>B001REEG6C</td>
      <td>A10EHUTGNC4BGP</td>
      <td>M. Foell</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2009-05-31</td>
      <td>Asparagus Bliss</td>
      <td>I love asparagus.  Up until very recently, I h...</td>
    </tr>
    <tr>
      <th>A10IFYN5U6X20R</th>
      <th>0</th>
      <td>2012-10-14</td>
      <td>248.0</td>
      <td>B007TFONH0</td>
      <td>A10IFYN5U6X20R</td>
      <td>onemagoo</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-14</td>
      <td>Best way to buy kcups</td>
      <td>Once you've tried some random sampler packs (a...</td>
    </tr>
    <tr>
      <th>A10RJEQN64ATXU</th>
      <th>0</th>
      <td>2007-09-03</td>
      <td>560.0</td>
      <td>B000G6RYNE</td>
      <td>A10RJEQN64ATXU</td>
      <td>Paul Rodney Williams "Higher Lifestyle"</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>2007-09-03</td>
      <td>delicious</td>
      <td>I have loved Kettle Brand Sea Salt and Vinegar...</td>
    </tr>
    <tr>
      <th>A11LJJL1SOH6W0</th>
      <th>0</th>
      <td>2011-01-29</td>
      <td>930.0</td>
      <td>B000ER6YO0</td>
      <td>A11LJJL1SOH6W0</td>
      <td>Sarah Ross "Mamasaurus Rex"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>2011-01-29</td>
      <td>My daughter's favorite jarred food</td>
      <td>My 7-1/2 month old absolutely loves this one.....</td>
    </tr>
    <tr>
      <th>A11QUNPSCNHY62</th>
      <th>0</th>
      <td>2012-06-17</td>
      <td>664.0</td>
      <td>B002BCD2OG</td>
      <td>A11QUNPSCNHY62</td>
      <td>N. R. Evans</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>2012-06-17</td>
      <td>Great substitute sweetener</td>
      <td>We have been using 17-Day Diet guided by Low-G...</td>
    </tr>
    <tr>
      <th>A11UPZ6LI1UJZY</th>
      <th>0</th>
      <td>2010-12-29</td>
      <td>932.0</td>
      <td>B000ER6YO0</td>
      <td>A11UPZ6LI1UJZY</td>
      <td>Pono</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2010-12-29</td>
      <td>the only jarred baby food my son ate</td>
      <td>From 6 month to 12 month, my son barely ate an...</td>
    </tr>
    <tr>
      <th>A11V8XN8SUHPOV</th>
      <th>0</th>
      <td>2010-01-20</td>
      <td>750.0</td>
      <td>B000Y2EJHY</td>
      <td>A11V8XN8SUHPOV</td>
      <td>Marilyn Demaio "mumsie"</td>
      <td>5.0</td>
      <td>10.0</td>
      <td>5.0</td>
      <td>2010-01-20</td>
      <td>cuttiest gum of the century</td>
      <td>this gum is super sick.tatooes are killin.flav...</td>
    </tr>
    <tr>
      <th>A11VPI6WLMQ2ZQ</th>
      <th>0</th>
      <td>2007-12-31</td>
      <td>871.0</td>
      <td>B000VKYKTG</td>
      <td>A11VPI6WLMQ2ZQ</td>
      <td>Lee A. Vercoe "TangSooPap"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2007-12-31</td>
      <td>Good stuff</td>
      <td>I had bought Pocky when on vacation and enjoye...</td>
    </tr>
    <tr>
      <th>A12OF4IM7A8YS</th>
      <th>0</th>
      <td>2008-07-05</td>
      <td>481.0</td>
      <td>B000G6RYNE</td>
      <td>A12OF4IM7A8YS</td>
      <td>Norman Tang "Amazon Norman"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2008-07-05</td>
      <td>Excellent balance of taste, crunchiness, and m...</td>
      <td>I simply fell in love with these chips and ref...</td>
    </tr>
    <tr>
      <th>A12T0F58OKHCIW</th>
      <th>0</th>
      <td>2007-07-10</td>
      <td>402.0</td>
      <td>B001ELL6O8</td>
      <td>A12T0F58OKHCIW</td>
      <td>fishaholic "hearyeethis"</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>2007-07-10</td>
      <td>poor item packaging</td>
      <td>This mix is very poorly packaged and breaks op...</td>
    </tr>
    <tr>
      <th>A132DJVI37RB4X</th>
      <th>0</th>
      <td>2012-03-22</td>
      <td>1000.0</td>
      <td>B006F2NYI2</td>
      <td>A132DJVI37RB4X</td>
      <td>Scottdrum</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2.0</td>
      <td>2012-03-22</td>
      <td>Not hot, not habanero</td>
      <td>I have to admit, I was a sucker for the large ...</td>
    </tr>
    <tr>
      <th>A137DV4YVGWDCR</th>
      <th>0</th>
      <td>2012-03-23</td>
      <td>317.0</td>
      <td>B000O9Y62A</td>
      <td>A137DV4YVGWDCR</td>
      <td>M. AIMINO</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-03-23</td>
      <td>Hot!</td>
      <td>It is hot! I love it, tasty and a little sweet...</td>
    </tr>
    <tr>
      <th>A139RTDNMU3WY5</th>
      <th>0</th>
      <td>2012-06-18</td>
      <td>376.0</td>
      <td>B0087HW5E2</td>
      <td>A139RTDNMU3WY5</td>
      <td>blanket lady</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2012-06-18</td>
      <td>Greatest Oil since slice bread !!!!!!!</td>
      <td>I have used this oil for several years and it ...</td>
    </tr>
    <tr>
      <th>A13EBBWOMC0FUI</th>
      <th>0</th>
      <td>2010-01-11</td>
      <td>936.0</td>
      <td>B000ER6YO0</td>
      <td>A13EBBWOMC0FUI</td>
      <td>Sherean Malekzadeh "Sherean"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2010-01-11</td>
      <td>One of my son's favorites</td>
      <td>My son started eating this when he was around ...</td>
    </tr>
    <tr>
      <th>A13G8CNY7VX57F</th>
      <th>0</th>
      <td>2011-12-03</td>
      <td>850.0</td>
      <td>B0041CKRJC</td>
      <td>A13G8CNY7VX57F</td>
      <td>Poppie</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2011-12-03</td>
      <td>This baking soda is the 'bomb!'</td>
      <td>I recently started using Bob's Red Mill Baking...</td>
    </tr>
    <tr>
      <th>A13HTFH4ZY0NCE</th>
      <th>0</th>
      <td>2010-12-01</td>
      <td>787.0</td>
      <td>B0018DQFPC</td>
      <td>A13HTFH4ZY0NCE</td>
      <td>NatalieVee</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2010-12-01</td>
      <td>Rudolph Gingerbread House</td>
      <td>I was pleasantly surprised to find this item h...</td>
    </tr>
    <tr>
      <th>A13KWQGEI9MHG0</th>
      <th>0</th>
      <td>2011-06-09</td>
      <td>986.0</td>
      <td>B00473OV2E</td>
      <td>A13KWQGEI9MHG0</td>
      <td>Book Dame</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-06-09</td>
      <td>Moore's Marinade, Gluten, low sodium and MSG F...</td>
      <td>This is a great product for those looking for ...</td>
    </tr>
    <tr>
      <th>A13SOXNMWXQVNU</th>
      <th>0</th>
      <td>2009-01-11</td>
      <td>526.0</td>
      <td>B000G6RYNE</td>
      <td>A13SOXNMWXQVNU</td>
      <td>ClickmeClickme</td>
      <td>4.0</td>
      <td>4.0</td>
      <td>5.0</td>
      <td>2009-01-11</td>
      <td>Chip snob alert!</td>
      <td>It feels strange to review chips, but I am com...</td>
    </tr>
    <tr>
      <th>A13T2G4T8LR8XA</th>
      <th>0</th>
      <td>2007-09-09</td>
      <td>395.0</td>
      <td>B001ELL6O8</td>
      <td>A13T2G4T8LR8XA</td>
      <td>First Time Mom</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2007-09-09</td>
      <td>Delisious Pancakes</td>
      <td>Before I discover this mix on Amazon I always ...</td>
    </tr>
    <tr>
      <th>A13VD7TJDWVOEC</th>
      <th>0</th>
      <td>2009-11-16</td>
      <td>457.0</td>
      <td>B000G6RYNE</td>
      <td>A13VD7TJDWVOEC</td>
      <td>Deborah E. Bumpus</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2009-11-16</td>
      <td>Best Kettle Chips!</td>
      <td>As a professional potato chip muncher, I see t...</td>
    </tr>
    <tr>
      <th>A1447CDAPZGLYV</th>
      <th>0</th>
      <td>2012-01-14</td>
      <td>336.0</td>
      <td>B00469VSJI</td>
      <td>A1447CDAPZGLYV</td>
      <td>SANA AWAR</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>2012-01-14</td>
      <td>No no</td>
      <td>Serveice delivery with the seller was excellen...</td>
    </tr>
    <tr>
      <th>A1465JH39KR5O5</th>
      <th>0</th>
      <td>2010-10-18</td>
      <td>453.0</td>
      <td>B000G6RYNE</td>
      <td>A1465JH39KR5O5</td>
      <td>persia</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2010-10-18</td>
      <td>Best deal ever!</td>
      <td>This was the best deal ever. The delivery was ...</td>
    </tr>
    <tr>
      <th>A14B8M117EUBLK</th>
      <th>0</th>
      <td>2007-06-01</td>
      <td>715.0</td>
      <td>B000G6MBX2</td>
      <td>A14B8M117EUBLK</td>
      <td>Shelly Kenyon</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2007-06-01</td>
      <td>Best tortilla chips ever!!!</td>
      <td>We like these chips for salsa and quacomole be...</td>
    </tr>
    <tr>
      <th>A14HZ5EMD2WCG</th>
      <th>0</th>
      <td>2008-01-06</td>
      <td>550.0</td>
      <td>B000G6RYNE</td>
      <td>A14HZ5EMD2WCG</td>
      <td>K. Schoeneman "Grape nut"</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>3.0</td>
      <td>2008-01-06</td>
      <td>Not as good as the English sell</td>
      <td>These are better in England. I don't know why....</td>
    </tr>
    <tr>
      <th>A14SOW889A4TAT</th>
      <th>0</th>
      <td>2009-12-29</td>
      <td>548.0</td>
      <td>B000G6RYNE</td>
      <td>A14SOW889A4TAT</td>
      <td>J. Tan</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>2009-12-29</td>
      <td>Over-fried</td>
      <td>I bought this brand as a trial since I am tire...</td>
    </tr>
    <tr>
      <th>A156LLD3VZJGC2</th>
      <th>0</th>
      <td>2012-10-19</td>
      <td>337.0</td>
      <td>B002SRAU80</td>
      <td>A156LLD3VZJGC2</td>
      <td>D. W. Knowlington</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-19</td>
      <td>The king of all seasoning salts.</td>
      <td>This is honestly THE BEST seasoning salt in al...</td>
    </tr>
    <tr>
      <th>A15U6JW86FB9OV</th>
      <th>0</th>
      <td>2008-06-28</td>
      <td>532.0</td>
      <td>B000G6RYNE</td>
      <td>A15U6JW86FB9OV</td>
      <td>P. Bangera</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>2008-06-28</td>
      <td>Best Chips out there!!!</td>
      <td>I have tried Herrs, UTZ, wise, pringles, lays ...</td>
    </tr>
    <tr>
      <th>A15UN7OK3WRFXE</th>
      <th>0</th>
      <td>2009-06-11</td>
      <td>699.0</td>
      <td>B000G6MBX2</td>
      <td>A15UN7OK3WRFXE</td>
      <td>Rich Wang</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2009-06-11</td>
      <td>OM NOM NOM NOM!</td>
      <td>These chips are delicious. If you like salt &amp; ...</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="19" valign="top">AY1EF0GOH80EK</th>
      <th>233</th>
      <td>2012-09-17</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>234</th>
      <td>2012-09-18</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>235</th>
      <td>2012-09-19</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>236</th>
      <td>2012-09-20</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>237</th>
      <td>2012-09-21</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>238</th>
      <td>2012-09-22</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>239</th>
      <td>2012-09-23</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>240</th>
      <td>2012-09-24</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>241</th>
      <td>2012-09-25</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>242</th>
      <td>2012-09-26</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>243</th>
      <td>2012-09-27</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>244</th>
      <td>2012-09-28</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>245</th>
      <td>2012-09-29</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>246</th>
      <td>2012-09-30</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>247</th>
      <td>2012-10-01</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>248</th>
      <td>2012-10-02</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>249</th>
      <td>2012-10-03</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>250</th>
      <td>2012-10-04</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>251</th>
      <td>2012-10-05</td>
      <td>650.0</td>
      <td>B001EPPCNK</td>
      <td>AY1EF0GOH80EK</td>
      <td>Natasha Stryker</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>2012-10-05</td>
      <td>Do not taste from bottle! Mix with vanilla for...</td>
      <td>So I got this and tasted it strait out of the ...</td>
    </tr>
    <tr>
      <th>AYAU6OMJ81Q7Z</th>
      <th>0</th>
      <td>2012-09-19</td>
      <td>918.0</td>
      <td>B000ER6YO0</td>
      <td>AYAU6OMJ81Q7Z</td>
      <td>Happinesslikeabutterfly</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-09-19</td>
      <td>Great stuff</td>
      <td>This is the only food my daughter will eat con...</td>
    </tr>
    <tr>
      <th>AYB4ELCS5AM8P</th>
      <th>0</th>
      <td>2011-03-20</td>
      <td>444.0</td>
      <td>B000G6RYNE</td>
      <td>AYB4ELCS5AM8P</td>
      <td>John B. Goode "JBG"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-03-20</td>
      <td>Excellent!</td>
      <td>What I like about them:&lt;br /&gt;1) Very thick chi...</td>
    </tr>
    <tr>
      <th>AYBYYDVV5ABJE</th>
      <th>0</th>
      <td>2009-08-26</td>
      <td>522.0</td>
      <td>B000G6RYNE</td>
      <td>AYBYYDVV5ABJE</td>
      <td>retrodog</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>3.0</td>
      <td>2009-08-26</td>
      <td>Too Much Flavor</td>
      <td>These things are just too darn cheesy. If you ...</td>
    </tr>
    <tr>
      <th>AYMV2T86WIXVD</th>
      <th>0</th>
      <td>2008-09-07</td>
      <td>857.0</td>
      <td>B000VKYKTG</td>
      <td>AYMV2T86WIXVD</td>
      <td>Nathan K. Arnold</td>
      <td>5.0</td>
      <td>5.0</td>
      <td>4.0</td>
      <td>2008-09-07</td>
      <td>awsome</td>
      <td>I looooove pocky, got once at lucky's and coul...</td>
    </tr>
    <tr>
      <th>AYSEXV27DPQ3E</th>
      <th>0</th>
      <td>2008-07-28</td>
      <td>607.0</td>
      <td>B000G6RYNE</td>
      <td>AYSEXV27DPQ3E</td>
      <td>Jilla "Jilla"</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2008-07-28</td>
      <td>Yummy!</td>
      <td>Tasted good. Spicy. Those that don't like spic...</td>
    </tr>
    <tr>
      <th>AYZZSRYAIXNOS</th>
      <th>0</th>
      <td>2011-01-31</td>
      <td>899.0</td>
      <td>B002DXZI40</td>
      <td>AYZZSRYAIXNOS</td>
      <td>I Heart Amazon "I heart Shopping"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-01-31</td>
      <td>I can't take the smell</td>
      <td>The product is all that it says it is which is...</td>
    </tr>
    <tr>
      <th>AZ2NEHE8TNRUW</th>
      <th>0</th>
      <td>2011-01-21</td>
      <td>644.0</td>
      <td>B001EPPFGO</td>
      <td>AZ2NEHE8TNRUW</td>
      <td>prb in California</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-01-21</td>
      <td>favorite Earl Grey tea</td>
      <td>After a lifetime of tea drinking, I can honest...</td>
    </tr>
    <tr>
      <th>AZ3FPU1QSFBC6</th>
      <th>0</th>
      <td>2011-03-13</td>
      <td>794.0</td>
      <td>B000UZMJZO</td>
      <td>AZ3FPU1QSFBC6</td>
      <td>annifree</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2011-03-13</td>
      <td>I love this tea - to each her own taste buds!</td>
      <td>I love the aroma and the taste of the fruit an...</td>
    </tr>
    <tr>
      <th>AZ7289G0ILRFF</th>
      <th>0</th>
      <td>2008-04-29</td>
      <td>398.0</td>
      <td>B001ELL6O8</td>
      <td>AZ7289G0ILRFF</td>
      <td>Stephanie Reid-Simons</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2008-04-29</td>
      <td>Perfect mix for egg-allergic!</td>
      <td>The pancakes taste great without any recipe ad...</td>
    </tr>
    <tr>
      <th>AZLONLC8OZPEC</th>
      <th>0</th>
      <td>2011-12-23</td>
      <td>82.0</td>
      <td>B0066DMI6Y</td>
      <td>AZLONLC8OZPEC</td>
      <td>John W. Hollis</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>2011-12-23</td>
      <td>Great</td>
      <td>Great gift for all ages! I purchased these gia...</td>
    </tr>
    <tr>
      <th>AZOF9E17RGZH8</th>
      <th>0</th>
      <td>2011-06-25</td>
      <td>22.0</td>
      <td>B001GVISJM</td>
      <td>AZOF9E17RGZH8</td>
      <td>Tammy Anderson</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-06-25</td>
      <td>TWIZZLERS</td>
      <td>I bought these for my husband who is currently...</td>
    </tr>
  </tbody>
</table>
<p>4068 rows × 11 columns</p>
</div>




```python
#EXAMPLE 9: build_df_per_id method: test without multiprocessing WITH multiple_date_operations = multiple_date_operations
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = False,
                             multiple_date_operations = multiple_date_operations)
new_df 
```

    Done with grouping 0.0002353191375732422
    Done 18.67437744140625





<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
    <tr>
      <th>UserId</th>
      <th>join_column_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A104Y49ZQ4CYJ2</th>
      <th>2012-08-13</th>
      <td>[171]</td>
      <td>[B0064KO0BU]</td>
      <td>A104Y49ZQ4CYJ2</td>
      <td>[H. Adams "hollya"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-08-13T00:00:00.000000000]</td>
      <td>[Worked great!]</td>
      <td>[I purchased as a giveaway for a baby shower w...</td>
    </tr>
    <tr>
      <th>A108P30XVUFKXY</th>
      <th>2008-02-19</th>
      <td>[51]</td>
      <td>[B001EO5QW8]</td>
      <td>A108P30XVUFKXY</td>
      <td>[Roberto A]</td>
      <td>0.0</td>
      <td>7.0</td>
      <td>1.0</td>
      <td>[2008-02-19T00:00:00.000000000]</td>
      <td>[Don't like it]</td>
      <td>[This oatmeal is not good. Its mushy, soft, I ...</td>
    </tr>
    <tr>
      <th>A10EHUTGNC4BGP</th>
      <th>2009-05-31</th>
      <td>[109]</td>
      <td>[B001REEG6C]</td>
      <td>A10EHUTGNC4BGP</td>
      <td>[M. Foell]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2009-05-31T00:00:00.000000000]</td>
      <td>[Asparagus Bliss]</td>
      <td>[I love asparagus.  Up until very recently, I ...</td>
    </tr>
    <tr>
      <th>A10IFYN5U6X20R</th>
      <th>2012-10-14</th>
      <td>[248]</td>
      <td>[B007TFONH0]</td>
      <td>A10IFYN5U6X20R</td>
      <td>[onemagoo]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-10-14T00:00:00.000000000]</td>
      <td>[Best way to buy kcups]</td>
      <td>[Once you've tried some random sampler packs (...</td>
    </tr>
    <tr>
      <th>A10RJEQN64ATXU</th>
      <th>2007-09-03</th>
      <td>[560]</td>
      <td>[B000G6RYNE]</td>
      <td>A10RJEQN64ATXU</td>
      <td>[Paul Rodney Williams "Higher Lifestyle"]</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>[2007-09-03T00:00:00.000000000]</td>
      <td>[delicious]</td>
      <td>[I have loved Kettle Brand Sea Salt and Vinega...</td>
    </tr>
    <tr>
      <th>A11LJJL1SOH6W0</th>
      <th>2011-01-29</th>
      <td>[930]</td>
      <td>[B000ER6YO0]</td>
      <td>A11LJJL1SOH6W0</td>
      <td>[Sarah Ross "Mamasaurus Rex"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>[2011-01-29T00:00:00.000000000]</td>
      <td>[My daughter's favorite jarred food]</td>
      <td>[My 7-1/2 month old absolutely loves this one....</td>
    </tr>
    <tr>
      <th>A11QUNPSCNHY62</th>
      <th>2012-06-17</th>
      <td>[664]</td>
      <td>[B002BCD2OG]</td>
      <td>A11QUNPSCNHY62</td>
      <td>[N. R. Evans]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>[2012-06-17T00:00:00.000000000]</td>
      <td>[Great substitute sweetener]</td>
      <td>[We have been using 17-Day Diet guided by Low-...</td>
    </tr>
    <tr>
      <th>A11UPZ6LI1UJZY</th>
      <th>2010-12-29</th>
      <td>[932]</td>
      <td>[B000ER6YO0]</td>
      <td>A11UPZ6LI1UJZY</td>
      <td>[Pono]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2010-12-29T00:00:00.000000000]</td>
      <td>[the only jarred baby food my son ate]</td>
      <td>[From 6 month to 12 month, my son barely ate a...</td>
    </tr>
    <tr>
      <th>A11V8XN8SUHPOV</th>
      <th>2010-01-20</th>
      <td>[750]</td>
      <td>[B000Y2EJHY]</td>
      <td>A11V8XN8SUHPOV</td>
      <td>[Marilyn Demaio "mumsie"]</td>
      <td>5.0</td>
      <td>10.0</td>
      <td>5.0</td>
      <td>[2010-01-20T00:00:00.000000000]</td>
      <td>[cuttiest gum of the century]</td>
      <td>[this gum is super sick.tatooes are killin.fla...</td>
    </tr>
    <tr>
      <th>A11VPI6WLMQ2ZQ</th>
      <th>2007-12-31</th>
      <td>[871]</td>
      <td>[B000VKYKTG]</td>
      <td>A11VPI6WLMQ2ZQ</td>
      <td>[Lee A. Vercoe "TangSooPap"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2007-12-31T00:00:00.000000000]</td>
      <td>[Good stuff]</td>
      <td>[I had bought Pocky when on vacation and enjoy...</td>
    </tr>
    <tr>
      <th>A12OF4IM7A8YS</th>
      <th>2008-07-05</th>
      <td>[481]</td>
      <td>[B000G6RYNE]</td>
      <td>A12OF4IM7A8YS</td>
      <td>[Norman Tang "Amazon Norman"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2008-07-05T00:00:00.000000000]</td>
      <td>[Excellent balance of taste, crunchiness, and ...</td>
      <td>[I simply fell in love with these chips and re...</td>
    </tr>
    <tr>
      <th>A12T0F58OKHCIW</th>
      <th>2007-07-10</th>
      <td>[402]</td>
      <td>[B001ELL6O8]</td>
      <td>A12T0F58OKHCIW</td>
      <td>[fishaholic "hearyeethis"]</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>[2007-07-10T00:00:00.000000000]</td>
      <td>[poor item packaging]</td>
      <td>[This mix is very poorly packaged and breaks o...</td>
    </tr>
    <tr>
      <th>A132DJVI37RB4X</th>
      <th>2012-03-22</th>
      <td>[1000]</td>
      <td>[B006F2NYI2]</td>
      <td>A132DJVI37RB4X</td>
      <td>[Scottdrum]</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2.0</td>
      <td>[2012-03-22T00:00:00.000000000]</td>
      <td>[Not hot, not habanero]</td>
      <td>[I have to admit, I was a sucker for the large...</td>
    </tr>
    <tr>
      <th>A137DV4YVGWDCR</th>
      <th>2012-03-23</th>
      <td>[317]</td>
      <td>[B000O9Y62A]</td>
      <td>A137DV4YVGWDCR</td>
      <td>[M. AIMINO]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-03-23T00:00:00.000000000]</td>
      <td>[Hot!]</td>
      <td>[It is hot! I love it, tasty and a little swee...</td>
    </tr>
    <tr>
      <th>A139RTDNMU3WY5</th>
      <th>2012-06-18</th>
      <td>[376]</td>
      <td>[B0087HW5E2]</td>
      <td>A139RTDNMU3WY5</td>
      <td>[blanket lady]</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>[2012-06-18T00:00:00.000000000]</td>
      <td>[Greatest Oil since slice bread !!!!!!!]</td>
      <td>[I have used this oil for several years and it...</td>
    </tr>
    <tr>
      <th>A13EBBWOMC0FUI</th>
      <th>2010-01-11</th>
      <td>[936]</td>
      <td>[B000ER6YO0]</td>
      <td>A13EBBWOMC0FUI</td>
      <td>[Sherean Malekzadeh "Sherean"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2010-01-11T00:00:00.000000000]</td>
      <td>[One of my son's favorites]</td>
      <td>[My son started eating this when he was around...</td>
    </tr>
    <tr>
      <th>A13G8CNY7VX57F</th>
      <th>2011-12-03</th>
      <td>[850]</td>
      <td>[B0041CKRJC]</td>
      <td>A13G8CNY7VX57F</td>
      <td>[Poppie]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2011-12-03T00:00:00.000000000]</td>
      <td>[This baking soda is the 'bomb!']</td>
      <td>[I recently started using Bob's Red Mill Bakin...</td>
    </tr>
    <tr>
      <th>A13HTFH4ZY0NCE</th>
      <th>2010-12-01</th>
      <td>[787]</td>
      <td>[B0018DQFPC]</td>
      <td>A13HTFH4ZY0NCE</td>
      <td>[NatalieVee]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2010-12-01T00:00:00.000000000]</td>
      <td>[Rudolph Gingerbread House]</td>
      <td>[I was pleasantly surprised to find this item ...</td>
    </tr>
    <tr>
      <th>A13KWQGEI9MHG0</th>
      <th>2011-06-09</th>
      <td>[986]</td>
      <td>[B00473OV2E]</td>
      <td>A13KWQGEI9MHG0</td>
      <td>[Book Dame]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-06-09T00:00:00.000000000]</td>
      <td>[Moore's Marinade, Gluten, low sodium and MSG ...</td>
      <td>[This is a great product for those looking for...</td>
    </tr>
    <tr>
      <th>A13SOXNMWXQVNU</th>
      <th>2009-01-11</th>
      <td>[526]</td>
      <td>[B000G6RYNE]</td>
      <td>A13SOXNMWXQVNU</td>
      <td>[ClickmeClickme]</td>
      <td>4.0</td>
      <td>4.0</td>
      <td>5.0</td>
      <td>[2009-01-11T00:00:00.000000000]</td>
      <td>[Chip snob alert!]</td>
      <td>[It feels strange to review chips, but I am co...</td>
    </tr>
    <tr>
      <th>A13T2G4T8LR8XA</th>
      <th>2007-09-09</th>
      <td>[395]</td>
      <td>[B001ELL6O8]</td>
      <td>A13T2G4T8LR8XA</td>
      <td>[First Time Mom]</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>[2007-09-09T00:00:00.000000000]</td>
      <td>[Delisious Pancakes]</td>
      <td>[Before I discover this mix on Amazon I always...</td>
    </tr>
    <tr>
      <th>A13VD7TJDWVOEC</th>
      <th>2009-11-16</th>
      <td>[457]</td>
      <td>[B000G6RYNE]</td>
      <td>A13VD7TJDWVOEC</td>
      <td>[Deborah E. Bumpus]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2009-11-16T00:00:00.000000000]</td>
      <td>[Best Kettle Chips!]</td>
      <td>[As a professional potato chip muncher, I see ...</td>
    </tr>
    <tr>
      <th>A1447CDAPZGLYV</th>
      <th>2012-01-14</th>
      <td>[336]</td>
      <td>[B00469VSJI]</td>
      <td>A1447CDAPZGLYV</td>
      <td>[SANA AWAR]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>[2012-01-14T00:00:00.000000000]</td>
      <td>[No no]</td>
      <td>[Serveice delivery with the seller was excelle...</td>
    </tr>
    <tr>
      <th>A1465JH39KR5O5</th>
      <th>2010-10-18</th>
      <td>[453]</td>
      <td>[B000G6RYNE]</td>
      <td>A1465JH39KR5O5</td>
      <td>[persia]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2010-10-18T00:00:00.000000000]</td>
      <td>[Best deal ever!]</td>
      <td>[This was the best deal ever. The delivery was...</td>
    </tr>
    <tr>
      <th>A14B8M117EUBLK</th>
      <th>2007-06-01</th>
      <td>[715]</td>
      <td>[B000G6MBX2]</td>
      <td>A14B8M117EUBLK</td>
      <td>[Shelly Kenyon]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2007-06-01T00:00:00.000000000]</td>
      <td>[Best tortilla chips ever!!!]</td>
      <td>[We like these chips for salsa and quacomole b...</td>
    </tr>
    <tr>
      <th>A14HZ5EMD2WCG</th>
      <th>2008-01-06</th>
      <td>[550]</td>
      <td>[B000G6RYNE]</td>
      <td>A14HZ5EMD2WCG</td>
      <td>[K. Schoeneman "Grape nut"]</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>3.0</td>
      <td>[2008-01-06T00:00:00.000000000]</td>
      <td>[Not as good as the English sell]</td>
      <td>[These are better in England. I don't know why...</td>
    </tr>
    <tr>
      <th>A14SOW889A4TAT</th>
      <th>2009-12-29</th>
      <td>[548]</td>
      <td>[B000G6RYNE]</td>
      <td>A14SOW889A4TAT</td>
      <td>[J. Tan]</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>[2009-12-29T00:00:00.000000000]</td>
      <td>[Over-fried]</td>
      <td>[I bought this brand as a trial since I am tir...</td>
    </tr>
    <tr>
      <th>A156LLD3VZJGC2</th>
      <th>2012-10-19</th>
      <td>[337]</td>
      <td>[B002SRAU80]</td>
      <td>A156LLD3VZJGC2</td>
      <td>[D. W. Knowlington]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-10-19T00:00:00.000000000]</td>
      <td>[The king of all seasoning salts.]</td>
      <td>[This is honestly THE BEST seasoning salt in a...</td>
    </tr>
    <tr>
      <th>A15U6JW86FB9OV</th>
      <th>2008-06-28</th>
      <td>[532]</td>
      <td>[B000G6RYNE]</td>
      <td>A15U6JW86FB9OV</td>
      <td>[P. Bangera]</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>[2008-06-28T00:00:00.000000000]</td>
      <td>[Best Chips out there!!!]</td>
      <td>[I have tried Herrs, UTZ, wise, pringles, lays...</td>
    </tr>
    <tr>
      <th>A15UN7OK3WRFXE</th>
      <th>2009-06-11</th>
      <td>[699]</td>
      <td>[B000G6MBX2]</td>
      <td>A15UN7OK3WRFXE</td>
      <td>[Rich Wang]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2009-06-11T00:00:00.000000000]</td>
      <td>[OM NOM NOM NOM!]</td>
      <td>[These chips are delicious. If you like salt &amp;...</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="19" valign="top">AY1EF0GOH80EK</th>
      <th>2012-09-17</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-18</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-19</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-20</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-21</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-22</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-23</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-24</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-25</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-26</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-27</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-28</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-29</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-30</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-01</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-02</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-03</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-04</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-05</th>
      <td>[650.0]</td>
      <td>[B001EPPCNK]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[Natasha Stryker]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>[2012-10-05T00:00:00.000000000]</td>
      <td>[Do not taste from bottle! Mix with vanilla fo...</td>
      <td>[So I got this and tasted it strait out of the...</td>
    </tr>
    <tr>
      <th>AYAU6OMJ81Q7Z</th>
      <th>2012-09-19</th>
      <td>[918]</td>
      <td>[B000ER6YO0]</td>
      <td>AYAU6OMJ81Q7Z</td>
      <td>[Happinesslikeabutterfly]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-09-19T00:00:00.000000000]</td>
      <td>[Great stuff]</td>
      <td>[This is the only food my daughter will eat co...</td>
    </tr>
    <tr>
      <th>AYB4ELCS5AM8P</th>
      <th>2011-03-20</th>
      <td>[444]</td>
      <td>[B000G6RYNE]</td>
      <td>AYB4ELCS5AM8P</td>
      <td>[John B. Goode "JBG"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-03-20T00:00:00.000000000]</td>
      <td>[Excellent!]</td>
      <td>[What I like about them:&lt;br /&gt;1) Very thick ch...</td>
    </tr>
    <tr>
      <th>AYBYYDVV5ABJE</th>
      <th>2009-08-26</th>
      <td>[522]</td>
      <td>[B000G6RYNE]</td>
      <td>AYBYYDVV5ABJE</td>
      <td>[retrodog]</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>3.0</td>
      <td>[2009-08-26T00:00:00.000000000]</td>
      <td>[Too Much Flavor]</td>
      <td>[These things are just too darn cheesy. If you...</td>
    </tr>
    <tr>
      <th>AYMV2T86WIXVD</th>
      <th>2008-09-07</th>
      <td>[857]</td>
      <td>[B000VKYKTG]</td>
      <td>AYMV2T86WIXVD</td>
      <td>[Nathan K. Arnold]</td>
      <td>5.0</td>
      <td>5.0</td>
      <td>4.0</td>
      <td>[2008-09-07T00:00:00.000000000]</td>
      <td>[awsome]</td>
      <td>[I looooove pocky, got once at lucky's and cou...</td>
    </tr>
    <tr>
      <th>AYSEXV27DPQ3E</th>
      <th>2008-07-28</th>
      <td>[607]</td>
      <td>[B000G6RYNE]</td>
      <td>AYSEXV27DPQ3E</td>
      <td>[Jilla "Jilla"]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2008-07-28T00:00:00.000000000]</td>
      <td>[Yummy!]</td>
      <td>[Tasted good. Spicy. Those that don't like spi...</td>
    </tr>
    <tr>
      <th>AYZZSRYAIXNOS</th>
      <th>2011-01-31</th>
      <td>[899]</td>
      <td>[B002DXZI40]</td>
      <td>AYZZSRYAIXNOS</td>
      <td>[I Heart Amazon "I heart Shopping"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-01-31T00:00:00.000000000]</td>
      <td>[I can't take the smell]</td>
      <td>[The product is all that it says it is which i...</td>
    </tr>
    <tr>
      <th>AZ2NEHE8TNRUW</th>
      <th>2011-01-21</th>
      <td>[644]</td>
      <td>[B001EPPFGO]</td>
      <td>AZ2NEHE8TNRUW</td>
      <td>[prb in California]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-01-21T00:00:00.000000000]</td>
      <td>[favorite Earl Grey tea]</td>
      <td>[After a lifetime of tea drinking, I can hones...</td>
    </tr>
    <tr>
      <th>AZ3FPU1QSFBC6</th>
      <th>2011-03-13</th>
      <td>[794]</td>
      <td>[B000UZMJZO]</td>
      <td>AZ3FPU1QSFBC6</td>
      <td>[annifree]</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2011-03-13T00:00:00.000000000]</td>
      <td>[I love this tea - to each her own taste buds!]</td>
      <td>[I love the aroma and the taste of the fruit a...</td>
    </tr>
    <tr>
      <th>AZ7289G0ILRFF</th>
      <th>2008-04-29</th>
      <td>[398]</td>
      <td>[B001ELL6O8]</td>
      <td>AZ7289G0ILRFF</td>
      <td>[Stephanie Reid-Simons]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2008-04-29T00:00:00.000000000]</td>
      <td>[Perfect mix for egg-allergic!]</td>
      <td>[The pancakes taste great without any recipe a...</td>
    </tr>
    <tr>
      <th>AZLONLC8OZPEC</th>
      <th>2011-12-23</th>
      <td>[82]</td>
      <td>[B0066DMI6Y]</td>
      <td>AZLONLC8OZPEC</td>
      <td>[John W. Hollis]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>[2011-12-23T00:00:00.000000000]</td>
      <td>[Great]</td>
      <td>[Great gift for all ages! I purchased these gi...</td>
    </tr>
    <tr>
      <th>AZOF9E17RGZH8</th>
      <th>2011-06-25</th>
      <td>[22]</td>
      <td>[B001GVISJM]</td>
      <td>AZOF9E17RGZH8</td>
      <td>[Tammy Anderson]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-06-25T00:00:00.000000000]</td>
      <td>[TWIZZLERS]</td>
      <td>[I bought these for my husband who is currentl...</td>
    </tr>
  </tbody>
</table>
<p>4050 rows × 10 columns</p>
</div>




```python
#EXAMPLE 10: build_df_per_id method: test WITH multiprocessing = True WITH multiple_date_operations = False
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = True,
                             multiple_date_operations = False)
new_df 
```

    Done with grouping 0.00024127960205078125
    Done with MultiProcessing 1.155691146850586
    Done 1.8582797050476074





<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>join_column_name</th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2012-08-13</td>
      <td>171.0</td>
      <td>B0064KO0BU</td>
      <td>A104Y49ZQ4CYJ2</td>
      <td>H. Adams "hollya"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-08-13</td>
      <td>Worked great!</td>
      <td>I purchased as a giveaway for a baby shower wi...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2008-02-19</td>
      <td>51.0</td>
      <td>B001EO5QW8</td>
      <td>A108P30XVUFKXY</td>
      <td>Roberto A</td>
      <td>0.0</td>
      <td>7.0</td>
      <td>1.0</td>
      <td>2008-02-19</td>
      <td>Don't like it</td>
      <td>This oatmeal is not good. Its mushy, soft, I d...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2009-05-31</td>
      <td>109.0</td>
      <td>B001REEG6C</td>
      <td>A10EHUTGNC4BGP</td>
      <td>M. Foell</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2009-05-31</td>
      <td>Asparagus Bliss</td>
      <td>I love asparagus.  Up until very recently, I h...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2012-10-14</td>
      <td>248.0</td>
      <td>B007TFONH0</td>
      <td>A10IFYN5U6X20R</td>
      <td>onemagoo</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-14</td>
      <td>Best way to buy kcups</td>
      <td>Once you've tried some random sampler packs (a...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2007-09-03</td>
      <td>560.0</td>
      <td>B000G6RYNE</td>
      <td>A10RJEQN64ATXU</td>
      <td>Paul Rodney Williams "Higher Lifestyle"</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>2007-09-03</td>
      <td>delicious</td>
      <td>I have loved Kettle Brand Sea Salt and Vinegar...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-01-29</td>
      <td>930.0</td>
      <td>B000ER6YO0</td>
      <td>A11LJJL1SOH6W0</td>
      <td>Sarah Ross "Mamasaurus Rex"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>2011-01-29</td>
      <td>My daughter's favorite jarred food</td>
      <td>My 7-1/2 month old absolutely loves this one.....</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2012-06-17</td>
      <td>664.0</td>
      <td>B002BCD2OG</td>
      <td>A11QUNPSCNHY62</td>
      <td>N. R. Evans</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>2012-06-17</td>
      <td>Great substitute sweetener</td>
      <td>We have been using 17-Day Diet guided by Low-G...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2010-12-29</td>
      <td>932.0</td>
      <td>B000ER6YO0</td>
      <td>A11UPZ6LI1UJZY</td>
      <td>Pono</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2010-12-29</td>
      <td>the only jarred baby food my son ate</td>
      <td>From 6 month to 12 month, my son barely ate an...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2010-01-20</td>
      <td>750.0</td>
      <td>B000Y2EJHY</td>
      <td>A11V8XN8SUHPOV</td>
      <td>Marilyn Demaio "mumsie"</td>
      <td>5.0</td>
      <td>10.0</td>
      <td>5.0</td>
      <td>2010-01-20</td>
      <td>cuttiest gum of the century</td>
      <td>this gum is super sick.tatooes are killin.flav...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2007-12-31</td>
      <td>871.0</td>
      <td>B000VKYKTG</td>
      <td>A11VPI6WLMQ2ZQ</td>
      <td>Lee A. Vercoe "TangSooPap"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2007-12-31</td>
      <td>Good stuff</td>
      <td>I had bought Pocky when on vacation and enjoye...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2008-07-05</td>
      <td>481.0</td>
      <td>B000G6RYNE</td>
      <td>A12OF4IM7A8YS</td>
      <td>Norman Tang "Amazon Norman"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2008-07-05</td>
      <td>Excellent balance of taste, crunchiness, and m...</td>
      <td>I simply fell in love with these chips and ref...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2007-07-10</td>
      <td>402.0</td>
      <td>B001ELL6O8</td>
      <td>A12T0F58OKHCIW</td>
      <td>fishaholic "hearyeethis"</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>2007-07-10</td>
      <td>poor item packaging</td>
      <td>This mix is very poorly packaged and breaks op...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2012-03-22</td>
      <td>1000.0</td>
      <td>B006F2NYI2</td>
      <td>A132DJVI37RB4X</td>
      <td>Scottdrum</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2.0</td>
      <td>2012-03-22</td>
      <td>Not hot, not habanero</td>
      <td>I have to admit, I was a sucker for the large ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2012-03-23</td>
      <td>317.0</td>
      <td>B000O9Y62A</td>
      <td>A137DV4YVGWDCR</td>
      <td>M. AIMINO</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-03-23</td>
      <td>Hot!</td>
      <td>It is hot! I love it, tasty and a little sweet...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2012-06-18</td>
      <td>376.0</td>
      <td>B0087HW5E2</td>
      <td>A139RTDNMU3WY5</td>
      <td>blanket lady</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2012-06-18</td>
      <td>Greatest Oil since slice bread !!!!!!!</td>
      <td>I have used this oil for several years and it ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2010-01-11</td>
      <td>936.0</td>
      <td>B000ER6YO0</td>
      <td>A13EBBWOMC0FUI</td>
      <td>Sherean Malekzadeh "Sherean"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2010-01-11</td>
      <td>One of my son's favorites</td>
      <td>My son started eating this when he was around ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-12-03</td>
      <td>850.0</td>
      <td>B0041CKRJC</td>
      <td>A13G8CNY7VX57F</td>
      <td>Poppie</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2011-12-03</td>
      <td>This baking soda is the 'bomb!'</td>
      <td>I recently started using Bob's Red Mill Baking...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2010-12-01</td>
      <td>787.0</td>
      <td>B0018DQFPC</td>
      <td>A13HTFH4ZY0NCE</td>
      <td>NatalieVee</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2010-12-01</td>
      <td>Rudolph Gingerbread House</td>
      <td>I was pleasantly surprised to find this item h...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-06-09</td>
      <td>986.0</td>
      <td>B00473OV2E</td>
      <td>A13KWQGEI9MHG0</td>
      <td>Book Dame</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-06-09</td>
      <td>Moore's Marinade, Gluten, low sodium and MSG F...</td>
      <td>This is a great product for those looking for ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2009-01-11</td>
      <td>526.0</td>
      <td>B000G6RYNE</td>
      <td>A13SOXNMWXQVNU</td>
      <td>ClickmeClickme</td>
      <td>4.0</td>
      <td>4.0</td>
      <td>5.0</td>
      <td>2009-01-11</td>
      <td>Chip snob alert!</td>
      <td>It feels strange to review chips, but I am com...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2007-09-09</td>
      <td>395.0</td>
      <td>B001ELL6O8</td>
      <td>A13T2G4T8LR8XA</td>
      <td>First Time Mom</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2007-09-09</td>
      <td>Delisious Pancakes</td>
      <td>Before I discover this mix on Amazon I always ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2009-11-16</td>
      <td>457.0</td>
      <td>B000G6RYNE</td>
      <td>A13VD7TJDWVOEC</td>
      <td>Deborah E. Bumpus</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2009-11-16</td>
      <td>Best Kettle Chips!</td>
      <td>As a professional potato chip muncher, I see t...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2012-01-14</td>
      <td>336.0</td>
      <td>B00469VSJI</td>
      <td>A1447CDAPZGLYV</td>
      <td>SANA AWAR</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>2012-01-14</td>
      <td>No no</td>
      <td>Serveice delivery with the seller was excellen...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2010-10-18</td>
      <td>453.0</td>
      <td>B000G6RYNE</td>
      <td>A1465JH39KR5O5</td>
      <td>persia</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2010-10-18</td>
      <td>Best deal ever!</td>
      <td>This was the best deal ever. The delivery was ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2007-06-01</td>
      <td>715.0</td>
      <td>B000G6MBX2</td>
      <td>A14B8M117EUBLK</td>
      <td>Shelly Kenyon</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2007-06-01</td>
      <td>Best tortilla chips ever!!!</td>
      <td>We like these chips for salsa and quacomole be...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2008-01-06</td>
      <td>550.0</td>
      <td>B000G6RYNE</td>
      <td>A14HZ5EMD2WCG</td>
      <td>K. Schoeneman "Grape nut"</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>3.0</td>
      <td>2008-01-06</td>
      <td>Not as good as the English sell</td>
      <td>These are better in England. I don't know why....</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2009-12-29</td>
      <td>548.0</td>
      <td>B000G6RYNE</td>
      <td>A14SOW889A4TAT</td>
      <td>J. Tan</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>2009-12-29</td>
      <td>Over-fried</td>
      <td>I bought this brand as a trial since I am tire...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2012-10-19</td>
      <td>337.0</td>
      <td>B002SRAU80</td>
      <td>A156LLD3VZJGC2</td>
      <td>D. W. Knowlington</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-10-19</td>
      <td>The king of all seasoning salts.</td>
      <td>This is honestly THE BEST seasoning salt in al...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2008-06-28</td>
      <td>532.0</td>
      <td>B000G6RYNE</td>
      <td>A15U6JW86FB9OV</td>
      <td>P. Bangera</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>2008-06-28</td>
      <td>Best Chips out there!!!</td>
      <td>I have tried Herrs, UTZ, wise, pringles, lays ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2009-06-11</td>
      <td>699.0</td>
      <td>B000G6MBX2</td>
      <td>A15UN7OK3WRFXE</td>
      <td>Rich Wang</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2009-06-11</td>
      <td>OM NOM NOM NOM!</td>
      <td>These chips are delicious. If you like salt &amp; ...</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>233</th>
      <td>2012-09-17</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>234</th>
      <td>2012-09-18</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>235</th>
      <td>2012-09-19</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>236</th>
      <td>2012-09-20</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>237</th>
      <td>2012-09-21</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>238</th>
      <td>2012-09-22</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>239</th>
      <td>2012-09-23</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>240</th>
      <td>2012-09-24</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>241</th>
      <td>2012-09-25</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>242</th>
      <td>2012-09-26</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>243</th>
      <td>2012-09-27</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>244</th>
      <td>2012-09-28</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>245</th>
      <td>2012-09-29</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>246</th>
      <td>2012-09-30</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>247</th>
      <td>2012-10-01</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>248</th>
      <td>2012-10-02</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>249</th>
      <td>2012-10-03</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>250</th>
      <td>2012-10-04</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaT</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>251</th>
      <td>2012-10-05</td>
      <td>650.0</td>
      <td>B001EPPCNK</td>
      <td>AY1EF0GOH80EK</td>
      <td>Natasha Stryker</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>2012-10-05</td>
      <td>Do not taste from bottle! Mix with vanilla for...</td>
      <td>So I got this and tasted it strait out of the ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2012-09-19</td>
      <td>918.0</td>
      <td>B000ER6YO0</td>
      <td>AYAU6OMJ81Q7Z</td>
      <td>Happinesslikeabutterfly</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2012-09-19</td>
      <td>Great stuff</td>
      <td>This is the only food my daughter will eat con...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-03-20</td>
      <td>444.0</td>
      <td>B000G6RYNE</td>
      <td>AYB4ELCS5AM8P</td>
      <td>John B. Goode "JBG"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-03-20</td>
      <td>Excellent!</td>
      <td>What I like about them:&lt;br /&gt;1) Very thick chi...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2009-08-26</td>
      <td>522.0</td>
      <td>B000G6RYNE</td>
      <td>AYBYYDVV5ABJE</td>
      <td>retrodog</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>3.0</td>
      <td>2009-08-26</td>
      <td>Too Much Flavor</td>
      <td>These things are just too darn cheesy. If you ...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2008-09-07</td>
      <td>857.0</td>
      <td>B000VKYKTG</td>
      <td>AYMV2T86WIXVD</td>
      <td>Nathan K. Arnold</td>
      <td>5.0</td>
      <td>5.0</td>
      <td>4.0</td>
      <td>2008-09-07</td>
      <td>awsome</td>
      <td>I looooove pocky, got once at lucky's and coul...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2008-07-28</td>
      <td>607.0</td>
      <td>B000G6RYNE</td>
      <td>AYSEXV27DPQ3E</td>
      <td>Jilla "Jilla"</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2008-07-28</td>
      <td>Yummy!</td>
      <td>Tasted good. Spicy. Those that don't like spic...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-01-31</td>
      <td>899.0</td>
      <td>B002DXZI40</td>
      <td>AYZZSRYAIXNOS</td>
      <td>I Heart Amazon "I heart Shopping"</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-01-31</td>
      <td>I can't take the smell</td>
      <td>The product is all that it says it is which is...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-01-21</td>
      <td>644.0</td>
      <td>B001EPPFGO</td>
      <td>AZ2NEHE8TNRUW</td>
      <td>prb in California</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-01-21</td>
      <td>favorite Earl Grey tea</td>
      <td>After a lifetime of tea drinking, I can honest...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-03-13</td>
      <td>794.0</td>
      <td>B000UZMJZO</td>
      <td>AZ3FPU1QSFBC6</td>
      <td>annifree</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2011-03-13</td>
      <td>I love this tea - to each her own taste buds!</td>
      <td>I love the aroma and the taste of the fruit an...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2008-04-29</td>
      <td>398.0</td>
      <td>B001ELL6O8</td>
      <td>AZ7289G0ILRFF</td>
      <td>Stephanie Reid-Simons</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>2008-04-29</td>
      <td>Perfect mix for egg-allergic!</td>
      <td>The pancakes taste great without any recipe ad...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-12-23</td>
      <td>82.0</td>
      <td>B0066DMI6Y</td>
      <td>AZLONLC8OZPEC</td>
      <td>John W. Hollis</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>2011-12-23</td>
      <td>Great</td>
      <td>Great gift for all ages! I purchased these gia...</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2011-06-25</td>
      <td>22.0</td>
      <td>B001GVISJM</td>
      <td>AZOF9E17RGZH8</td>
      <td>Tammy Anderson</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2011-06-25</td>
      <td>TWIZZLERS</td>
      <td>I bought these for my husband who is currently...</td>
    </tr>
  </tbody>
</table>
<p>4068 rows × 11 columns</p>
</div>




```python
#EXAMPLE 11: build_df_per_id method: test WITH multiprocessing = True WITH multiple_date_operations = True
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = True,
                             multiple_date_operations = True)
new_df 
```

    Done with grouping 0.00027179718017578125
    Done with MultiProcessing 1.0583584308624268
    Done 1.2386565208435059





<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Id</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>UserId</th>
    </tr>
    <tr>
      <th>join_column_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2012-08-13</th>
      <td>171.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A104Y49ZQ4CYJ2</td>
    </tr>
    <tr>
      <th>2008-02-19</th>
      <td>51.0</td>
      <td>0.0</td>
      <td>7.0</td>
      <td>1.0</td>
      <td>A108P30XVUFKXY</td>
    </tr>
    <tr>
      <th>2009-05-31</th>
      <td>109.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>A10EHUTGNC4BGP</td>
    </tr>
    <tr>
      <th>2012-10-14</th>
      <td>248.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A10IFYN5U6X20R</td>
    </tr>
    <tr>
      <th>2007-09-03</th>
      <td>560.0</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>A10RJEQN64ATXU</td>
    </tr>
    <tr>
      <th>2011-01-29</th>
      <td>930.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>A11LJJL1SOH6W0</td>
    </tr>
    <tr>
      <th>2012-06-17</th>
      <td>664.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>A11QUNPSCNHY62</td>
    </tr>
    <tr>
      <th>2010-12-29</th>
      <td>932.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A11UPZ6LI1UJZY</td>
    </tr>
    <tr>
      <th>2010-01-20</th>
      <td>750.0</td>
      <td>5.0</td>
      <td>10.0</td>
      <td>5.0</td>
      <td>A11V8XN8SUHPOV</td>
    </tr>
    <tr>
      <th>2007-12-31</th>
      <td>871.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A11VPI6WLMQ2ZQ</td>
    </tr>
    <tr>
      <th>2008-07-05</th>
      <td>481.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A12OF4IM7A8YS</td>
    </tr>
    <tr>
      <th>2007-07-10</th>
      <td>402.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>A12T0F58OKHCIW</td>
    </tr>
    <tr>
      <th>2012-03-22</th>
      <td>1000.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2.0</td>
      <td>A132DJVI37RB4X</td>
    </tr>
    <tr>
      <th>2012-03-23</th>
      <td>317.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A137DV4YVGWDCR</td>
    </tr>
    <tr>
      <th>2012-06-18</th>
      <td>376.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>A139RTDNMU3WY5</td>
    </tr>
    <tr>
      <th>2010-01-11</th>
      <td>936.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A13EBBWOMC0FUI</td>
    </tr>
    <tr>
      <th>2011-12-03</th>
      <td>850.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>A13G8CNY7VX57F</td>
    </tr>
    <tr>
      <th>2010-12-01</th>
      <td>787.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A13HTFH4ZY0NCE</td>
    </tr>
    <tr>
      <th>2011-06-09</th>
      <td>986.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A13KWQGEI9MHG0</td>
    </tr>
    <tr>
      <th>2009-01-11</th>
      <td>526.0</td>
      <td>4.0</td>
      <td>4.0</td>
      <td>5.0</td>
      <td>A13SOXNMWXQVNU</td>
    </tr>
    <tr>
      <th>2007-09-09</th>
      <td>395.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>A13T2G4T8LR8XA</td>
    </tr>
    <tr>
      <th>2009-11-16</th>
      <td>457.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A13VD7TJDWVOEC</td>
    </tr>
    <tr>
      <th>2012-01-14</th>
      <td>336.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>A1447CDAPZGLYV</td>
    </tr>
    <tr>
      <th>2010-10-18</th>
      <td>453.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A1465JH39KR5O5</td>
    </tr>
    <tr>
      <th>2007-06-01</th>
      <td>715.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A14B8M117EUBLK</td>
    </tr>
    <tr>
      <th>2008-01-06</th>
      <td>550.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>3.0</td>
      <td>A14HZ5EMD2WCG</td>
    </tr>
    <tr>
      <th>2009-12-29</th>
      <td>548.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>A14SOW889A4TAT</td>
    </tr>
    <tr>
      <th>2012-10-19</th>
      <td>337.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A156LLD3VZJGC2</td>
    </tr>
    <tr>
      <th>2008-06-28</th>
      <td>532.0</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>A15U6JW86FB9OV</td>
    </tr>
    <tr>
      <th>2009-06-11</th>
      <td>699.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>A15UN7OK3WRFXE</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2012-09-17</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-18</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-19</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-20</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-21</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-22</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-23</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-24</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-25</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-26</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-27</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-28</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-29</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-30</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-02</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-03</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-04</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-10-05</th>
      <td>650.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>AY1EF0GOH80EK</td>
    </tr>
    <tr>
      <th>2012-09-19</th>
      <td>918.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AYAU6OMJ81Q7Z</td>
    </tr>
    <tr>
      <th>2011-03-20</th>
      <td>444.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AYB4ELCS5AM8P</td>
    </tr>
    <tr>
      <th>2009-08-26</th>
      <td>522.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>3.0</td>
      <td>AYBYYDVV5ABJE</td>
    </tr>
    <tr>
      <th>2008-09-07</th>
      <td>857.0</td>
      <td>5.0</td>
      <td>5.0</td>
      <td>4.0</td>
      <td>AYMV2T86WIXVD</td>
    </tr>
    <tr>
      <th>2008-07-28</th>
      <td>607.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>AYSEXV27DPQ3E</td>
    </tr>
    <tr>
      <th>2011-01-31</th>
      <td>899.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AYZZSRYAIXNOS</td>
    </tr>
    <tr>
      <th>2011-01-21</th>
      <td>644.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AZ2NEHE8TNRUW</td>
    </tr>
    <tr>
      <th>2011-03-13</th>
      <td>794.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>AZ3FPU1QSFBC6</td>
    </tr>
    <tr>
      <th>2008-04-29</th>
      <td>398.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>AZ7289G0ILRFF</td>
    </tr>
    <tr>
      <th>2011-12-23</th>
      <td>82.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>AZLONLC8OZPEC</td>
    </tr>
    <tr>
      <th>2011-06-25</th>
      <td>22.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>AZOF9E17RGZH8</td>
    </tr>
  </tbody>
</table>
<p>4050 rows × 5 columns</p>
</div>




```python
#EXAMPLE 12: build_df_per_id method: test WITH multiprocessing = True WITH multiple_date_operations = True
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = True,
                             multiple_date_operations = multiple_date_operations)
new_df 
```

    Done with grouping 0.00022935867309570312
    Done with MultiProcessing 2.0858078002929688
    Done 2.4349660873413086





<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Id</th>
      <th>ProductId</th>
      <th>UserId</th>
      <th>ProfileName</th>
      <th>HelpfulnessNumerator</th>
      <th>HelpfulnessDenominator</th>
      <th>Score</th>
      <th>Time</th>
      <th>Summary</th>
      <th>Text</th>
    </tr>
    <tr>
      <th>join_column_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2012-08-13</th>
      <td>[171]</td>
      <td>[B0064KO0BU]</td>
      <td>A104Y49ZQ4CYJ2</td>
      <td>[H. Adams "hollya"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-08-13T00:00:00.000000000]</td>
      <td>[Worked great!]</td>
      <td>[I purchased as a giveaway for a baby shower w...</td>
    </tr>
    <tr>
      <th>2008-02-19</th>
      <td>[51]</td>
      <td>[B001EO5QW8]</td>
      <td>A108P30XVUFKXY</td>
      <td>[Roberto A]</td>
      <td>0.0</td>
      <td>7.0</td>
      <td>1.0</td>
      <td>[2008-02-19T00:00:00.000000000]</td>
      <td>[Don't like it]</td>
      <td>[This oatmeal is not good. Its mushy, soft, I ...</td>
    </tr>
    <tr>
      <th>2009-05-31</th>
      <td>[109]</td>
      <td>[B001REEG6C]</td>
      <td>A10EHUTGNC4BGP</td>
      <td>[M. Foell]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2009-05-31T00:00:00.000000000]</td>
      <td>[Asparagus Bliss]</td>
      <td>[I love asparagus.  Up until very recently, I ...</td>
    </tr>
    <tr>
      <th>2012-10-14</th>
      <td>[248]</td>
      <td>[B007TFONH0]</td>
      <td>A10IFYN5U6X20R</td>
      <td>[onemagoo]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-10-14T00:00:00.000000000]</td>
      <td>[Best way to buy kcups]</td>
      <td>[Once you've tried some random sampler packs (...</td>
    </tr>
    <tr>
      <th>2007-09-03</th>
      <td>[560]</td>
      <td>[B000G6RYNE]</td>
      <td>A10RJEQN64ATXU</td>
      <td>[Paul Rodney Williams "Higher Lifestyle"]</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>[2007-09-03T00:00:00.000000000]</td>
      <td>[delicious]</td>
      <td>[I have loved Kettle Brand Sea Salt and Vinega...</td>
    </tr>
    <tr>
      <th>2011-01-29</th>
      <td>[930]</td>
      <td>[B000ER6YO0]</td>
      <td>A11LJJL1SOH6W0</td>
      <td>[Sarah Ross "Mamasaurus Rex"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>[2011-01-29T00:00:00.000000000]</td>
      <td>[My daughter's favorite jarred food]</td>
      <td>[My 7-1/2 month old absolutely loves this one....</td>
    </tr>
    <tr>
      <th>2012-06-17</th>
      <td>[664]</td>
      <td>[B002BCD2OG]</td>
      <td>A11QUNPSCNHY62</td>
      <td>[N. R. Evans]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>[2012-06-17T00:00:00.000000000]</td>
      <td>[Great substitute sweetener]</td>
      <td>[We have been using 17-Day Diet guided by Low-...</td>
    </tr>
    <tr>
      <th>2010-12-29</th>
      <td>[932]</td>
      <td>[B000ER6YO0]</td>
      <td>A11UPZ6LI1UJZY</td>
      <td>[Pono]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2010-12-29T00:00:00.000000000]</td>
      <td>[the only jarred baby food my son ate]</td>
      <td>[From 6 month to 12 month, my son barely ate a...</td>
    </tr>
    <tr>
      <th>2010-01-20</th>
      <td>[750]</td>
      <td>[B000Y2EJHY]</td>
      <td>A11V8XN8SUHPOV</td>
      <td>[Marilyn Demaio "mumsie"]</td>
      <td>5.0</td>
      <td>10.0</td>
      <td>5.0</td>
      <td>[2010-01-20T00:00:00.000000000]</td>
      <td>[cuttiest gum of the century]</td>
      <td>[this gum is super sick.tatooes are killin.fla...</td>
    </tr>
    <tr>
      <th>2007-12-31</th>
      <td>[871]</td>
      <td>[B000VKYKTG]</td>
      <td>A11VPI6WLMQ2ZQ</td>
      <td>[Lee A. Vercoe "TangSooPap"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2007-12-31T00:00:00.000000000]</td>
      <td>[Good stuff]</td>
      <td>[I had bought Pocky when on vacation and enjoy...</td>
    </tr>
    <tr>
      <th>2008-07-05</th>
      <td>[481]</td>
      <td>[B000G6RYNE]</td>
      <td>A12OF4IM7A8YS</td>
      <td>[Norman Tang "Amazon Norman"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2008-07-05T00:00:00.000000000]</td>
      <td>[Excellent balance of taste, crunchiness, and ...</td>
      <td>[I simply fell in love with these chips and re...</td>
    </tr>
    <tr>
      <th>2007-07-10</th>
      <td>[402]</td>
      <td>[B001ELL6O8]</td>
      <td>A12T0F58OKHCIW</td>
      <td>[fishaholic "hearyeethis"]</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>[2007-07-10T00:00:00.000000000]</td>
      <td>[poor item packaging]</td>
      <td>[This mix is very poorly packaged and breaks o...</td>
    </tr>
    <tr>
      <th>2012-03-22</th>
      <td>[1000]</td>
      <td>[B006F2NYI2]</td>
      <td>A132DJVI37RB4X</td>
      <td>[Scottdrum]</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>2.0</td>
      <td>[2012-03-22T00:00:00.000000000]</td>
      <td>[Not hot, not habanero]</td>
      <td>[I have to admit, I was a sucker for the large...</td>
    </tr>
    <tr>
      <th>2012-03-23</th>
      <td>[317]</td>
      <td>[B000O9Y62A]</td>
      <td>A137DV4YVGWDCR</td>
      <td>[M. AIMINO]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-03-23T00:00:00.000000000]</td>
      <td>[Hot!]</td>
      <td>[It is hot! I love it, tasty and a little swee...</td>
    </tr>
    <tr>
      <th>2012-06-18</th>
      <td>[376]</td>
      <td>[B0087HW5E2]</td>
      <td>A139RTDNMU3WY5</td>
      <td>[blanket lady]</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>[2012-06-18T00:00:00.000000000]</td>
      <td>[Greatest Oil since slice bread !!!!!!!]</td>
      <td>[I have used this oil for several years and it...</td>
    </tr>
    <tr>
      <th>2010-01-11</th>
      <td>[936]</td>
      <td>[B000ER6YO0]</td>
      <td>A13EBBWOMC0FUI</td>
      <td>[Sherean Malekzadeh "Sherean"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2010-01-11T00:00:00.000000000]</td>
      <td>[One of my son's favorites]</td>
      <td>[My son started eating this when he was around...</td>
    </tr>
    <tr>
      <th>2011-12-03</th>
      <td>[850]</td>
      <td>[B0041CKRJC]</td>
      <td>A13G8CNY7VX57F</td>
      <td>[Poppie]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2011-12-03T00:00:00.000000000]</td>
      <td>[This baking soda is the 'bomb!']</td>
      <td>[I recently started using Bob's Red Mill Bakin...</td>
    </tr>
    <tr>
      <th>2010-12-01</th>
      <td>[787]</td>
      <td>[B0018DQFPC]</td>
      <td>A13HTFH4ZY0NCE</td>
      <td>[NatalieVee]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2010-12-01T00:00:00.000000000]</td>
      <td>[Rudolph Gingerbread House]</td>
      <td>[I was pleasantly surprised to find this item ...</td>
    </tr>
    <tr>
      <th>2011-06-09</th>
      <td>[986]</td>
      <td>[B00473OV2E]</td>
      <td>A13KWQGEI9MHG0</td>
      <td>[Book Dame]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-06-09T00:00:00.000000000]</td>
      <td>[Moore's Marinade, Gluten, low sodium and MSG ...</td>
      <td>[This is a great product for those looking for...</td>
    </tr>
    <tr>
      <th>2009-01-11</th>
      <td>[526]</td>
      <td>[B000G6RYNE]</td>
      <td>A13SOXNMWXQVNU</td>
      <td>[ClickmeClickme]</td>
      <td>4.0</td>
      <td>4.0</td>
      <td>5.0</td>
      <td>[2009-01-11T00:00:00.000000000]</td>
      <td>[Chip snob alert!]</td>
      <td>[It feels strange to review chips, but I am co...</td>
    </tr>
    <tr>
      <th>2007-09-09</th>
      <td>[395]</td>
      <td>[B001ELL6O8]</td>
      <td>A13T2G4T8LR8XA</td>
      <td>[First Time Mom]</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>5.0</td>
      <td>[2007-09-09T00:00:00.000000000]</td>
      <td>[Delisious Pancakes]</td>
      <td>[Before I discover this mix on Amazon I always...</td>
    </tr>
    <tr>
      <th>2009-11-16</th>
      <td>[457]</td>
      <td>[B000G6RYNE]</td>
      <td>A13VD7TJDWVOEC</td>
      <td>[Deborah E. Bumpus]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2009-11-16T00:00:00.000000000]</td>
      <td>[Best Kettle Chips!]</td>
      <td>[As a professional potato chip muncher, I see ...</td>
    </tr>
    <tr>
      <th>2012-01-14</th>
      <td>[336]</td>
      <td>[B00469VSJI]</td>
      <td>A1447CDAPZGLYV</td>
      <td>[SANA AWAR]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>[2012-01-14T00:00:00.000000000]</td>
      <td>[No no]</td>
      <td>[Serveice delivery with the seller was excelle...</td>
    </tr>
    <tr>
      <th>2010-10-18</th>
      <td>[453]</td>
      <td>[B000G6RYNE]</td>
      <td>A1465JH39KR5O5</td>
      <td>[persia]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2010-10-18T00:00:00.000000000]</td>
      <td>[Best deal ever!]</td>
      <td>[This was the best deal ever. The delivery was...</td>
    </tr>
    <tr>
      <th>2007-06-01</th>
      <td>[715]</td>
      <td>[B000G6MBX2]</td>
      <td>A14B8M117EUBLK</td>
      <td>[Shelly Kenyon]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2007-06-01T00:00:00.000000000]</td>
      <td>[Best tortilla chips ever!!!]</td>
      <td>[We like these chips for salsa and quacomole b...</td>
    </tr>
    <tr>
      <th>2008-01-06</th>
      <td>[550]</td>
      <td>[B000G6RYNE]</td>
      <td>A14HZ5EMD2WCG</td>
      <td>[K. Schoeneman "Grape nut"]</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>3.0</td>
      <td>[2008-01-06T00:00:00.000000000]</td>
      <td>[Not as good as the English sell]</td>
      <td>[These are better in England. I don't know why...</td>
    </tr>
    <tr>
      <th>2009-12-29</th>
      <td>[548]</td>
      <td>[B000G6RYNE]</td>
      <td>A14SOW889A4TAT</td>
      <td>[J. Tan]</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>1.0</td>
      <td>[2009-12-29T00:00:00.000000000]</td>
      <td>[Over-fried]</td>
      <td>[I bought this brand as a trial since I am tir...</td>
    </tr>
    <tr>
      <th>2012-10-19</th>
      <td>[337]</td>
      <td>[B002SRAU80]</td>
      <td>A156LLD3VZJGC2</td>
      <td>[D. W. Knowlington]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-10-19T00:00:00.000000000]</td>
      <td>[The king of all seasoning salts.]</td>
      <td>[This is honestly THE BEST seasoning salt in a...</td>
    </tr>
    <tr>
      <th>2008-06-28</th>
      <td>[532]</td>
      <td>[B000G6RYNE]</td>
      <td>A15U6JW86FB9OV</td>
      <td>[P. Bangera]</td>
      <td>3.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>[2008-06-28T00:00:00.000000000]</td>
      <td>[Best Chips out there!!!]</td>
      <td>[I have tried Herrs, UTZ, wise, pringles, lays...</td>
    </tr>
    <tr>
      <th>2009-06-11</th>
      <td>[699]</td>
      <td>[B000G6MBX2]</td>
      <td>A15UN7OK3WRFXE</td>
      <td>[Rich Wang]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2009-06-11T00:00:00.000000000]</td>
      <td>[OM NOM NOM NOM!]</td>
      <td>[These chips are delicious. If you like salt &amp;...</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2012-09-17</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-18</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-19</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-20</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-21</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-22</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-23</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-24</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-25</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-26</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-27</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-28</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-29</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-09-30</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-01</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-02</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-03</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-04</th>
      <td>[nan]</td>
      <td>[nan]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[nan]</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>[NaT]</td>
      <td>[nan]</td>
      <td>[nan]</td>
    </tr>
    <tr>
      <th>2012-10-05</th>
      <td>[650.0]</td>
      <td>[B001EPPCNK]</td>
      <td>AY1EF0GOH80EK</td>
      <td>[Natasha Stryker]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>4.0</td>
      <td>[2012-10-05T00:00:00.000000000]</td>
      <td>[Do not taste from bottle! Mix with vanilla fo...</td>
      <td>[So I got this and tasted it strait out of the...</td>
    </tr>
    <tr>
      <th>2012-09-19</th>
      <td>[918]</td>
      <td>[B000ER6YO0]</td>
      <td>AYAU6OMJ81Q7Z</td>
      <td>[Happinesslikeabutterfly]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2012-09-19T00:00:00.000000000]</td>
      <td>[Great stuff]</td>
      <td>[This is the only food my daughter will eat co...</td>
    </tr>
    <tr>
      <th>2011-03-20</th>
      <td>[444]</td>
      <td>[B000G6RYNE]</td>
      <td>AYB4ELCS5AM8P</td>
      <td>[John B. Goode "JBG"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-03-20T00:00:00.000000000]</td>
      <td>[Excellent!]</td>
      <td>[What I like about them:&lt;br /&gt;1) Very thick ch...</td>
    </tr>
    <tr>
      <th>2009-08-26</th>
      <td>[522]</td>
      <td>[B000G6RYNE]</td>
      <td>AYBYYDVV5ABJE</td>
      <td>[retrodog]</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>3.0</td>
      <td>[2009-08-26T00:00:00.000000000]</td>
      <td>[Too Much Flavor]</td>
      <td>[These things are just too darn cheesy. If you...</td>
    </tr>
    <tr>
      <th>2008-09-07</th>
      <td>[857]</td>
      <td>[B000VKYKTG]</td>
      <td>AYMV2T86WIXVD</td>
      <td>[Nathan K. Arnold]</td>
      <td>5.0</td>
      <td>5.0</td>
      <td>4.0</td>
      <td>[2008-09-07T00:00:00.000000000]</td>
      <td>[awsome]</td>
      <td>[I looooove pocky, got once at lucky's and cou...</td>
    </tr>
    <tr>
      <th>2008-07-28</th>
      <td>[607]</td>
      <td>[B000G6RYNE]</td>
      <td>AYSEXV27DPQ3E</td>
      <td>[Jilla "Jilla"]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2008-07-28T00:00:00.000000000]</td>
      <td>[Yummy!]</td>
      <td>[Tasted good. Spicy. Those that don't like spi...</td>
    </tr>
    <tr>
      <th>2011-01-31</th>
      <td>[899]</td>
      <td>[B002DXZI40]</td>
      <td>AYZZSRYAIXNOS</td>
      <td>[I Heart Amazon "I heart Shopping"]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-01-31T00:00:00.000000000]</td>
      <td>[I can't take the smell]</td>
      <td>[The product is all that it says it is which i...</td>
    </tr>
    <tr>
      <th>2011-01-21</th>
      <td>[644]</td>
      <td>[B001EPPFGO]</td>
      <td>AZ2NEHE8TNRUW</td>
      <td>[prb in California]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-01-21T00:00:00.000000000]</td>
      <td>[favorite Earl Grey tea]</td>
      <td>[After a lifetime of tea drinking, I can hones...</td>
    </tr>
    <tr>
      <th>2011-03-13</th>
      <td>[794]</td>
      <td>[B000UZMJZO]</td>
      <td>AZ3FPU1QSFBC6</td>
      <td>[annifree]</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2011-03-13T00:00:00.000000000]</td>
      <td>[I love this tea - to each her own taste buds!]</td>
      <td>[I love the aroma and the taste of the fruit a...</td>
    </tr>
    <tr>
      <th>2008-04-29</th>
      <td>[398]</td>
      <td>[B001ELL6O8]</td>
      <td>AZ7289G0ILRFF</td>
      <td>[Stephanie Reid-Simons]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>5.0</td>
      <td>[2008-04-29T00:00:00.000000000]</td>
      <td>[Perfect mix for egg-allergic!]</td>
      <td>[The pancakes taste great without any recipe a...</td>
    </tr>
    <tr>
      <th>2011-12-23</th>
      <td>[82]</td>
      <td>[B0066DMI6Y]</td>
      <td>AZLONLC8OZPEC</td>
      <td>[John W. Hollis]</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>4.0</td>
      <td>[2011-12-23T00:00:00.000000000]</td>
      <td>[Great]</td>
      <td>[Great gift for all ages! I purchased these gi...</td>
    </tr>
    <tr>
      <th>2011-06-25</th>
      <td>[22]</td>
      <td>[B001GVISJM]</td>
      <td>AZOF9E17RGZH8</td>
      <td>[Tammy Anderson]</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>[2011-06-25T00:00:00.000000000]</td>
      <td>[TWIZZLERS]</td>
      <td>[I bought these for my husband who is currentl...</td>
    </tr>
  </tbody>
</table>
<p>4050 rows × 10 columns</p>
</div>


