import time
import pandas as pd
import numpy as np
from src.redis_dataframe import DataFrameRedis
from src.table_timeseries import TableTimeSeries
import logging

logging.basicConfig(filename='/home/casper/repos2/example.log',level=logging.DEBUG)

def get_new_dates(df, date_col):
    TTS = TableTimeSeries()
    df[date_col] = df[date_col].apply(TTS.break_date).fillna('0'*8)
    df = TTS.recast_date_column(df, date_col)
    return df

def get_features(ndf, column_operations, day):
    new_df = pd.DataFrame()
    for col, operations in column_operations.items():
        for operation in operations:
            val = getattr(ndf[col].fillna(0), operation)()
            new_df['{}_{}_{}'.format(col, operation, day)] = [val]
    return new_df


def calc_temp_features(df, date_column, date, days, column_operations, USER_ID):
    df_list = []
    for day in days[::-1]:
        df = df.loc[(df[date_column] >= pd.to_datetime(date) - pd.Timedelta(days = day)) &
                       (df[date_column] < pd.to_datetime(date))]    
        feature_df = get_features(df, column_operations, day)
        df_list.append(feature_df)
        
    df = pd.concat(df_list, axis = 1)
    df['new_date_column'] = [date]
    df['msno'] = [USER_ID]
    return df

def churn_trans_func(args):
    #START DEFAULT
    group, date_column, min_resolution, id_column = args
    TTS = TableTimeSeries()
    time_df =  TTS.build_df(group, date_column, min_resolution)
    #END DEFAULT
    
    #Temporal features args
    last_date = time_df[date_column].values[-1]
    days = [30, 60, 120, 180, 360, 720, 1080]
    column_operations = {
        'payment_method_id': ['nunique'], #Also add as static variable?
        'payment_plan_days': ['mean'],
        'plan_list_price':['sum', 'mean', 'nunique'],
        'actual_amount_paid':['sum', 'mean', 'nunique'],
        'is_auto_renew': ['nunique', 'sum'],
        'is_cancel': ['nunique', 'sum'],
    }
    user_id = time_df[id_column].values[0]
    trans_temp_df = calc_temp_features(time_df, date_column, last_date, days, column_operations, user_id)
    trans_temp_df = trans_temp_df.drop('new_date_column', axis = 1)
    return trans_temp_df


def churn_logs_func(args):
    #START DEFAULT
    group, date_column, min_resolution, id_column = args
    TTS = TableTimeSeries()
    time_df =  TTS.build_df(group, date_column, min_resolution)
    #END DEFAULT
    
    #Temporal features args
    last_date = time_df[date_column].values[-1]
    days = [30, 60, 120, 180, 360, 720, 1080]

    column_operations = {
        'num_25': ['sum', 'mean', 'nunique', 'max', 'std'],
        'num_50': ['sum', 'mean', 'nunique', 'max', 'std'],
        'num_75': ['sum', 'mean', 'nunique', 'max', 'std'],
        'num_985': ['sum', 'mean', 'nunique', 'max', 'std'],
        'num_100': ['sum', 'mean', 'nunique', 'max', 'std'],
        'num_unq': ['sum', 'mean', 'nunique', 'max', 'std'],
        'total_secs': ['mean', 'max', 'std'],
    }
    user_id = time_df[id_column].values[0]
    trans_temp_df = calc_temp_features(time_df, last_date, days, column_operations, user_id)
    trans_temp_df = trans_temp_df.drop('new_date_column', axis = 1)
    return trans_temp_df




s = time.time()
DFR = DataFrameRedis()

################################
##### Work on transactions
################################

transactions = DFR.get_df('churn_transactions')
logging.debug('Opened transactions df {}'.format(time.time() - s))
              
new_transactions = get_new_dates(transactions, 'transaction_date')
logging.debug('new dates for transactions df {}'.format(time.time() - s))

unique_user_ids = new_transactions.msno.unique()
user_id_chunks = np.array_split(unique_user_ids, 100)
logging.debug('split user_ids {}'.format(time.time() - s))


TTS = TableTimeSeries()
DFR = DataFrameRedis(_flush_df = False, use_multiprocessing=True)
for i, user_id_chunk in enumerate(user_id_chunks):
    df_slice = new_transactions.loc[new_transactions.msno.isin(user_id_chunk)]
    temp_trans_data = TTS.build_df_per_id(df_slice, 'transaction_date', 'day', 'msno', use_multiprocessing=True, custom_func=churn_trans_func)
    DFR.set_df('churn_temporal_transactions', temp_trans_data)   
    logging.debug('mega_temporal features transactions df chunk {} / {} in {}'.format(i, len(user_id_chunks), time.time() - s))


    
################################
##### Work on User Logs
################################
dfs = pd.read_csv('/mnt/datasets/datasets/_kaggle churn data/user_logs.csv', chunksize = 10**7)

for i, df in enumerate(dfs):
    new_log_df = get_new_dates(df, 'date')
    logging.debug('new dates user logs part{} in {}'.format(i, time.time() - s))
    TTS = TableTimeSeries()
    temp_trans_data = TTS.build_df_per_id(new_log_df, 'date', 'day', 'msno', use_multiprocessing=True, custom_func=churn_logs_func)
    logging.debug('written temporal user logs part{} in {}'.format(i, time.time() - s))
    DFR.set_df('churn_temporal_log_files', temp_trans_data)
    
logging.debug('concat temporal user logs in {}'.format(time.time() - s))
